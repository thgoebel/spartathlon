# Releasing

This document outlines the steps needed for releasing a new version of the Spartathlon app.

1. Test all views in all themes on all available devices.

2. Update both the `CHANGELOG.md` and the `fastlane/metadata/android/en-US/changelogs/$VERSIONCODE.txt` files

3. Bump the version number and version code both in the `AboutView.dart` and in the `build.gradle`

4. Merge everything to main: `git checkout main && git merge dev`

5. Tag the new version: `git tag v0.2.0 $COMMIT_HASH`

6. Push everything: `git push && git push --tags`

7. [F-Droid] If this release needs to be build with a new Flutter version, send them an MR:

    * Clone their repo (https://gitlab.com/fdroid/fdroiddata) and create a NEW branch

    * Edit the config file at `metadata/ch.thgoebel.spartathlonapp.yml`
