import 'package:flutter/material.dart';

import 'package:spartathlon_app/models/Athlete.dart';

/// Defines a custom tile, similar to a ListTile, but so that "leading", "title",
/// and "trailing" all use the same font size.
///
/// Can be used in to display a table of an athlete's past finishes
class CustomFinishTile extends StatelessWidget {
  CustomFinishTile(this.f, {Key? key}) : super(key: key);

  final Finish f;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
      ),
      padding: EdgeInsets.only(top: 12.0, left: 16.0, right: 16.0, bottom: 8.0),
      child: Row(
        children: [
          Container(
            width: 50.0,
            child: Text(f.year),
          ),
          Expanded(
            child: Container(
              child: Text(f.time),
            ),
          ),
          Container(
            child: Text(f.bib),
          ),
        ],
      ),
    );
  }
}
