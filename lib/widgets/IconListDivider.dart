import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:spartathlon_app/util/Themes.dart';
import 'package:spartathlon_app/util/Utils.dart';

/// List Divider that has an icon in the middle and dividers tp both side of that.
class IconListDivider extends StatelessWidget {
  IconListDivider(this.state, this.icon, {Key? key}) : super(key: key);

  final SliverStickyHeaderState state;
  final Icon icon;

  // padding to the far left, far right and to both side of the icon
  final double padding = 16.0;

  @override
  Widget build(BuildContext context) {
    bool isHovered = state.isPinned && state.scrollPercentage == 0.0;
    double bottomBorderThickness = isLight(Theme.of(context)) ? 1.0 : 1.5;

    return Container(
      decoration: isHovered
          ? BoxDecoration(color: Theme.of(context).cardColor, boxShadow: [
              BoxShadow(blurRadius: kShadowBlurRadius, color: Colors.black45)
            ])
          : BoxDecoration(color: Theme.of(context).scaffoldBackgroundColor),
      height: 36.0 + bottomBorderThickness,
      child: Column(
        children: [
          Row(
            children: [
              Container(width: padding),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 10.0, right: 10.0),
                  height: 1.5,
                  color: Theme.of(context).dividerColor,
                ),
              ),
              Container(width: padding),
              Container(
                  padding: EdgeInsets.only(top: 6.0, bottom: 6.0), child: icon),
              Container(width: padding),
              Expanded(
                child: Container(
                  height: 1.5,
                  color: Theme.of(context).dividerColor,
                ),
              ),
              Container(width: padding),
            ],
          ),
          isHovered
              ? Container(
                  height: bottomBorderThickness,
                  color: Theme.of(context).dividerColor,
                )
              : Container()
        ],
      ),
    );
  }
}
