import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/providers/CpProvider.dart';
import 'package:spartathlon_app/util/RaceTiming.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/views/AthleteDetailsView.dart';
import 'package:spartathlon_app/widgets/SpartaCard.dart';

/// A square card in the style of a Material Card whose width and height are
/// given by [cardSize].
///
/// It displays the athlete image and when tapped opens the [athlete]'s full details.
///
/// If the race is currently underway, an overlay at the bottom also shows the
/// time and location where the athlete was last seen.
///
class FavAthleteCard extends StatelessWidget {
  FavAthleteCard(this.athlete, this.cardSize, {Key? key}) : super(key: key);

  final Athlete athlete;

  // Hardcoding isn't necessarily the most elegant way of doing this,
  // we may find a better way in the future.
  // Problem: Get this FavAthleteCard to try and be as small as possible and
  // not use the whole screen width if placed in  a ListView
  final double cardSize;

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: athlete.name,
      child: Container(
        constraints: BoxConstraints(
          // height is constrained by the ListView's height
          maxHeight: cardSize,
          maxWidth: cardSize,
          // TODO: minWidth == maxWidth (bounded constraint) should be equal max(size(row_place), size(row_time))
        ),
        child: SpartaCard(
          margin: EdgeInsets.only(left: 5.0, right: 5.0),
          child: Stack(
            // Only contains card under ink overlay
            children: _getStackChildren(context, cardSize),
          ),
        ),
      ),
    );
  }

  List<Widget> _getStackChildren(BuildContext context, double cardMaxSize) {
    Color overlayBackgroundColor = Color(0x79000000);
    Color overlayTextColor = Colors.white; //Colors.black87;

    List<Widget> stackChildren = [
      // Athlete image in the background
      ClipRRect(
        borderRadius: BorderRadius.circular(kCardCornerRadius),
        child: CachedNetworkImage(
          imageUrl: athlete.imageUrl,
          width: cardMaxSize,
          height: cardMaxSize,
          fit: BoxFit.cover,
          alignment: Alignment(0, 0.80),
          placeholder: ((context, _) =>
              Container(color: Theme.of(context).primaryColor)),
        ),
      ),
    ];

    // Add the live timing overlay
    if (isRaceOn()) {
      stackChildren.add(Positioned(
        width: cardMaxSize - 18,
        left: 0,
        bottom: 0,
        // top: 0, // uncomment to cover whole card with overlay
        child: ClipRRect(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(kCardCornerRadius),
            bottomRight: Radius.circular(kCardCornerRadius),
          ),
          child: Container(
            width: cardMaxSize,
            padding:
                EdgeInsets.only(left: 8.0, right: 8.0, top: 6.0, bottom: 6.0),
            color: overlayBackgroundColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                (athlete.lastSeenCp.compareTo(75) == 0)
                    // Display different icon and name if athlete has finished
                    ? Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(right: 8.0),
                            child:
                                Icon(Icons.outlined_flag, color: Colors.white),
                          ),
                          Text(
                            'Finish',
                            style: TextStyle(color: overlayTextColor),
                          )
                        ],
                      )
                    : Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(right: 8.0),
                            child: Icon(Icons.place_outlined, color: Colors.white),
                          ),
                          Text(
                            'CP ' +
                                athlete.lastSeenCp.toString() +
                                CpProvider.getCpLocation(athlete.lastSeenCp),
                            style: TextStyle(color: overlayTextColor),
                          ),
                        ],
                      ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(top: 4.0, right: 8.0),
                      child: Icon(
                        Icons.access_time,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      athlete.lastSeenTime,
                      style: TextStyle(color: overlayTextColor),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ));
    }

    // Foreground overlay for onTap animation
    stackChildren.add(Material(
      type: MaterialType.transparency,
      child: InkWell(
          borderRadius: BorderRadius.circular(kCardCornerRadius),
          onTap: () {
            Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => AthleteDetailsView(athlete: athlete),
                ));
          }),
    ));

    return stackChildren;
  }
}
