import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:spartathlon_app/models/BlogPost.dart';
import 'package:spartathlon_app/util/Themes.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/views/BlogFullView.dart';
import 'package:spartathlon_app/widgets/SpartaCard.dart';

/// A BlogEntryCard in the style of a Material Card but adapted to the use case
/// of header image, title and starting lines
class BlogEntryCard extends StatelessWidget {
  BlogEntryCard(this.item, {Key? key}) : super(key: key);

  final BlogPost item;

  @override
  Widget build(BuildContext context) {
    // Previously: MediaQuery.of(context).size.width / 3.3;
    // Now: enforce fixed height (needed for fixed number of lines)
    double cardHeight = 115;

    return Container(
      constraints: BoxConstraints.tight(
          Size(MediaQuery.of(context).size.width - 32.0, cardHeight)),
      child: SpartaCard(
        margin: EdgeInsets.only(left: 10.0, bottom: 10.0, right: 10.0),
        child: Stack(
          children: [
            Container(
              child: Row(
                children: [
                  if (item.imageUrl != null)
                    Container(
                      constraints: BoxConstraints.loose(
                        Size(0.9 * cardHeight, cardHeight),
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(kCardCornerRadius),
                            bottomLeft: Radius.circular(kCardCornerRadius)),
                        child: CachedNetworkImage(
                          imageUrl: item.imageUrl!,
                          // null-checked above
                          fit: BoxFit.cover,
                          // TODO Upgrade to cached_network_image 0.7.0
                          // Workaround for https://github.com/renefloor/flutter_cached_network_image/issues/134
                          width: double.infinity,
                          height: double.infinity,
                          placeholder: ((context, _) =>
                              Container(color: Theme.of(context).primaryColor)),
                        ),
                      ),
                    ),
                  Expanded(
                    child: Container(
                      constraints: BoxConstraints.tight(
                        Size.fromWidth(MediaQuery.of(context).size.width -
                            32 -
                            cardHeight -
                            10.0),
                      ),
                      padding:
                          EdgeInsets.only(top: 6.0, left: 10.0, right: 8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            item.title,
                            maxLines: 1,
                            softWrap: false,
                            // https://github.com/flutter/flutter/issues/71413
                            overflow: (kIsWeb)
                                ? TextOverflow.ellipsis
                                : TextOverflow.fade,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15.0),
                          ),
                          Text(
                            item.teaserText,
                            maxLines: 4,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: secondaryTextColor(context),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Material(
              type: MaterialType.transparency,
              child: InkWell(
                borderRadius: BorderRadius.circular(kCardCornerRadius),
                onTap: () {
                  Navigator.push(
                    context,
                    CupertinoPageRoute(
                        builder: (context) => BlogFullView(item: item)),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
