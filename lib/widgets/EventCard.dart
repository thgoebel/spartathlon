import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:intl/intl.dart';
import 'package:platform/platform.dart';
import 'package:spartathlon_app/models/Event.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/widgets/SpartaCard.dart';
import 'package:spartathlon_app/widgets/SpartaDivider.dart';

class EventCard extends StatelessWidget {
  const EventCard({required this.events, Key? key}) : super(key: key);

  final List<Event> events;

  @override
  Widget build(BuildContext context) {
    return SpartaCard(
      margin: EdgeInsets.only(left: 10.0, top: 4.0, right: 10.0, bottom: 4.0),
      child: _getBody(context),
    );
  }

  Widget _getBody(BuildContext context) {
    List<Widget> children = [
      Container(
        padding: EdgeInsets.all(12.0),
        child: Text(
          DateFormat(DateFormat.YEAR_MONTH_WEEKDAY_DAY)
              .format(events.first.from),
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      SpartaDivider(),
    ];

    for (Event event in events) {
      String time = DateFormat('HH:mm').format(event.from.toLocal());
      if (event.to != null) {
        time += ' - ' + DateFormat('HH:mm').format(event.to!.toLocal());
      }

      children.add(
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 12.0, top: 10.0),
              constraints: BoxConstraints(minWidth: 100.0),
              child: Text(time),
            ),
            Expanded(
              child: Container(
                constraints: BoxConstraints(
                  maxWidth: (event.coordinates.isNotEmpty)
                      ? (MediaQuery.of(context).size.width - 160)
                      : (MediaQuery.of(context).size.width - 110),
                ),
                padding: EdgeInsets.only(left: 6.0, top: 10.0, bottom: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(event.description),
                    (event.location.isNotEmpty)
                        ? Text(
                            event.location,
                            style:
                                TextStyle(color: Colors.grey, fontSize: 12.0),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
            (event.coordinates.isNotEmpty)
                ? IconButton(
                    icon: Icon(
                      Icons.directions_outlined,
                      color: Theme.of(context).primaryColor,
                    ),
                    onPressed: () {
                      String geoUrl = getGeoUrl(
                          event.coordinates.split(',')[0].trim(),
                          event.coordinates.split(',')[1].trim(),
                          LocalPlatform());
                      launchUrl(geoUrl);
                    },
                    tooltip: SpartaLocalizations.of(context).tooltipOpenInMaps,
                  )
                : Container(),
          ],
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: children,
    );
  }
}
