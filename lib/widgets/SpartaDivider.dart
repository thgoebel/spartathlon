import 'package:flutter/material.dart';
import 'package:spartathlon_app/util/Themes.dart';

/// A divider that can take different widths
class SpartaDivider extends StatelessWidget {
  SpartaDivider({this.width, Key? key});

  final double? width;

  @override
  Widget build(BuildContext context) {
    double? widthToUse = width;
    if (widthToUse == null) {
      widthToUse = isLight(Theme.of(context)) ? 0.5 : 0.7;
    }

    return Container(
      height: 0.0,
      decoration: BoxDecoration(
        border: Border(
          bottom:
              BorderSide(width: widthToUse, color: Theme.of(context).dividerColor),
        ),
      ),
    );
  }
}
