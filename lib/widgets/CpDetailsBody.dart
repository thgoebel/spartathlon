import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:platform/platform.dart';
import 'package:spartathlon_app/models/Cp.dart';
import 'package:spartathlon_app/util/DistanceUnit.dart';
import 'package:spartathlon_app/util/Utils.dart';

/// Widget showing CP details.
/// Used e.g. as a body for the expanded ExpansionTile
class CpDetailsBody extends StatelessWidget {
  CpDetailsBody({
    required this.cp,
    required this.distanceUnit,
    Key? key,
  }) : super(key: key);

  final Cp cp;
  final DistanceUnit distanceUnit;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 0.0, bottom: 16.0),
      child: Column(
        children: [
          Row(
            // Top row with distances
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 4.0),
                    child: Icon(Icons.account_balance),
                  ),
                  Text(getDistanceString(context, cp.fromStart, distanceUnit)),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 4.0),
                    child: Icon(Icons.redo),
                  ),
                  Text(getDistanceString(context, cp.toNext, distanceUnit)),
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 4.0),
                    child: Icon(Icons.flag),
                  ),
                  Text(getDistanceString(context, cp.toFinish, distanceUnit)),
                ],
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 4.0),
                      child: Icon(Icons.access_time),
                    ),
                    Text(cp.openingTime + '﹣' + cp.closingTime),
                  ],
                ),
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 16.0),
                      child: Icon(Icons.local_drink_outlined),
                    ),
                    Flexible(
                      child: Text(
                        cp.supplies,
                        maxLines: 100,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Icon(Icons.info_outline),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(right: 4.0),
                    child: Text(
                      cp.description,
                      maxLines: 25,
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                    ),
                  ),
                ),
                ElevatedButton.icon(
                  icon: Icon(Icons.directions),
                  label: Text(SpartaLocalizations.of(context).labelButtonRoute),
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                  ),
                  onPressed: () {
                    String geoUrl = getGeoUrl(
                        cp.lat.toString(), cp.long.toString(), LocalPlatform());
                    launchUrl(geoUrl);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
