import 'package:flutter/material.dart';
import 'package:spartathlon_app/util/Utils.dart';

/// Generic reusable card in this app's style
/// which automatically applies the correct border and shadow

class SpartaCard extends StatelessWidget {
  SpartaCard({
    required this.margin,
    required this.child,
    Key? key,
  }) : super(key: key);

  final EdgeInsets margin;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(kCardCornerRadius)),
          color: Theme.of(context).cardColor,
          elevation: kCardElevation,
          child: child),
    );
  }
}
