import 'package:flutter/material.dart';
import 'package:spartathlon_app/util/Themes.dart';
import 'package:spartathlon_app/util/Utils.dart';

/// Yellow notification bar with a warning text and retry button
class ErrorBar extends StatelessWidget {
  ErrorBar({
    required this.errorText,
    required this.retryText,
    required this.onRetryPressed,
  });

  final String errorText;
  final String retryText;
  final VoidCallback onRetryPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16.0, right: 2.0),
      height: kNoNetworkBarHeight,
      decoration: BoxDecoration(
        color: spartaYellowDark,
        boxShadow: [
          BoxShadow(blurRadius: kShadowBlurRadius, color: Colors.black45)
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            errorText,
            style: TextStyle(color: Colors.black),
          ),
          TextButton(
            onPressed: onRetryPressed,
            child: Text(
              retryText.toUpperCase(),
              style: TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    );
  }
}
