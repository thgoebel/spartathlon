import 'package:flutter/material.dart';

/// An AthleteDetailsListTile in the style of a Material ListTile.
/// Differences:
/// - stripped down to the functionality we need
/// - leading icon is centered (not top-aligned like ListTile)
/// - supertitle instead of subtitle
class AthleteDetailsListTile extends StatelessWidget {
  AthleteDetailsListTile({
    Key? key,
    required this.leading,
    required this.title,
    required this.supertitle,
  }) : super(key: key);

  final Widget leading;
  final Text title;
  final Text supertitle;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 64.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 20.0, right: 20.0),
              child: leading,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                supertitle,
                title,
              ],
            )
          ],
        ));
  }
}

TextStyle supertitleStyle = TextStyle(
  color: Colors.grey,
  fontSize: 12.0,
);

TextStyle titleStyle = TextStyle(
  fontSize: 16.0,
);
