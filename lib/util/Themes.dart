import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';

String themeModeToString(BuildContext context, ThemeMode mode) {
  switch (mode) {
    case ThemeMode.light:
      return SpartaLocalizations.of(context).themeLight;
    case ThemeMode.dark:
      return SpartaLocalizations.of(context).themeDark;
    default:
      return SpartaLocalizations.of(context).themeFollowSystem;
  }
}

bool isLight(ThemeData theme) {
  // TODO Better discriminator of themes that scales to more than two?
  return theme.brightness == Brightness.light;
}

/// Custom colors
const Color spartaBlue = Color(0xFF005596);
const Color spartaBlueDark = Color(0xFF2962FF);
const Color spartaYellowDark = Color(0xFFFFC629);

// See ColorScheme issue: https://github.com/flutter/flutter/issues/83148

/// Light theme
final lightTextSelectionThemeColor = Colors.black54;
final ThemeData lightTheme = ThemeData.light().copyWith(
  // because our primaryColor is dark
  appBarTheme: AppBarTheme(
    systemOverlayStyle: SystemUiOverlayStyle.light,
    backgroundColor: spartaBlue,
  ),
  colorScheme: ColorScheme.light().copyWith(
    primary: spartaBlue,
    secondary: spartaBlue,
  ),
  primaryColor: spartaBlue,
  primaryColorLight: Colors.white,
  accentColor: spartaBlue,
  canvasColor: Colors.grey[50],
  cardColor: Colors.white,
  dialogBackgroundColor: Colors.white,
  scaffoldBackgroundColor: Colors.grey[100],
  textTheme: Typography.material2018(platform: defaultTargetPlatform).black,
  toggleableActiveColor: spartaBlue,
  iconTheme: IconThemeData(color: spartaBlue),
  dividerColor: Colors.grey[400],
);

/// Dark theme
// Guideline: https://material.io/design/color/dark-theme.html
final darkTextSelectionThemeColor = Colors.grey[300];
final ThemeData darkTheme = ThemeData.dark().copyWith(
  appBarTheme: AppBarTheme(
      backgroundColor: spartaBlueDark,
  ),
  brightness: Brightness.dark,
  colorScheme: ColorScheme.dark().copyWith(
    primary: spartaBlueDark,
    secondary: spartaBlueDark,
  ),
  primaryColor: spartaBlueDark,
  primaryColorLight: spartaBlueDark,
  accentColor: spartaBlueDark,
  canvasColor: Colors.grey[850],
  cardColor: Color(0xFF242424),
  dialogBackgroundColor: Colors.grey[900],
  scaffoldBackgroundColor: Color(0xFF121212),
  textTheme: Typography.material2018(platform: defaultTargetPlatform).white,
  toggleableActiveColor: spartaYellowDark,
  iconTheme: IconThemeData(color: spartaBlueDark),
  dividerColor: Color(0x40FFFFFF),
);

// TODO Easier access to custom attributes/colors through extending ThemeData?

Color markerColor(BuildContext context) =>
    isLight(Theme.of(context)) ? Colors.black : Colors.white;

// Black + white with 50% transparency
Color markerColorLight(BuildContext context) =>
    isLight(Theme.of(context)) ? Color(0x80000000) : Color(0x80FFFFFF);

Color secondaryTextColor(BuildContext context) =>
    isLight(Theme.of(context)) ? Colors.black54 : Colors.grey[300]!;
