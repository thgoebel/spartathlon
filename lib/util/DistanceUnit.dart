import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum DistanceUnit { km, miles }

const String SETTING_DISTANCE = 'distanceUnit';

/// Get the DistanceUnit preference from disk
Future<DistanceUnit> getDistanceUnitFromPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int unitId = prefs.getInt(SETTING_DISTANCE) ?? DistanceUnit.km.index;
  return DistanceUnit.values[unitId];
}

/// Stores [unit] to the preferences on disk
Future<void> setDistanceToPrefs(DistanceUnit unit) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  print('Setting distance unit to ' + unit.toString());
  await prefs.setInt(SETTING_DISTANCE, unit.index);
  // Ignore failure to save, e.g. on web
}

/// Returns a human-readable string representation of [unit]
String distanceUnitToString(BuildContext? context, DistanceUnit unit) {
  // Allow context to be null to enable testing without mocking the context
  switch (unit) {
    case DistanceUnit.km:
      return (context != null)
          ? SpartaLocalizations.of(context).distanceUnitKm
          : 'km';
    case DistanceUnit.miles:
      return (context != null)
          ? SpartaLocalizations.of(context).distanceUnitMiles
          : 'miles';
    default:
      return 'Unknown';
  }
}

/// Returns a human-readable string from a [distanceInKm] and a [unit]
/// Values are rounded to one decimal place
String getDistanceString(
    BuildContext? context, double distanceInKm, DistanceUnit unit) {
  // Round to one decimal place
  String val = distanceInKm.toUnit(unit).toStringAsFixed(1);
  String unitString = distanceUnitToString(context, unit);
  return "$val $unitString";
}

extension DistanceUnitExt on double {
  // Assumes KM to be the baseline
  double toUnit(DistanceUnit unit) {
    double distanceInKm = this;
    switch (unit) {
      case DistanceUnit.km:
        return distanceInKm;
      case DistanceUnit.miles:
        return 0.6213712 * distanceInKm;
      default:
        return distanceInKm;
    }
  }
}
