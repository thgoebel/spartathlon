// Use clock.now() instead of DateTime.now() to ease testing
import 'package:clock/clock.dart';

/// Return a [DateTime] object with Spartathlon's start time for the given [year]
DateTime raceStart(int year) {
  // The race starts at 7am local time, which is 4am UTC
  switch (year) {
    case 2018:
      return DateTime.utc(2018, 9, 28, 4);
    case 2019:
      return DateTime.utc(2019, 9, 27, 4);
    case 2020:
      return DateTime.utc(2020, 9, 25, 4);
    case 2021:
      return DateTime.utc(2021, 9, 24, 4);
    default:
      return DateTime.now().toUtc().subtract(Duration(days: 1));
  }
}

/// Return a [DateTime] object with Spartathlon's end time for the given [year]
DateTime raceEnd(int year) {
  // The race ends at 7pm / 19:00 local time, which is 16:00 UTC
  switch (year) {
    case 2018:
      return DateTime.utc(2018, 9, 29, 16);
    case 2019:
      return DateTime.utc(2019, 9, 28, 16);
    case 2020:
      return DateTime.utc(2020, 9, 26, 16);
    case 2021:
      return DateTime.utc(2021, 9, 25, 16);
    default:
      return DateTime.now().toUtc().add(Duration(days: 1));
  }
}

/// Return true if [a] is after [b] but before [c]
///
/// Expects all parameters to be in UTC
bool isBetween(DateTime a, DateTime b, DateTime c) {
  // Ensure all all or none are in UTC
  assert(a.isUtc && b.isUtc && c.isUtc);
  return a.isAfter(b) && a.isBefore(c);
}

/// Returns true if the Spartathletes are currently running
bool isRaceOn({clock = const Clock()}) {
  DateTime now = clock.now().toUtc();
  int year = now.year;
  return isBetween(now, raceStart(year), raceEnd(year));
}

/// Return true if Spartathlon is taking place this week
///
/// "This week" being from the Monday before the race until the Tuesday after
/// (considering all the events around the race).
bool isRaceWeek({clock = const Clock()}) {
  DateTime now = clock.now().toUtc();
  DateTime weekStart = raceStart(now.year).subtract(Duration(days: 5));
  DateTime weekEnd = raceEnd(now.year).add(Duration(days: 4));
  return isBetween(now, weekStart, weekEnd);
}

/// Returns true if Spartathlon has just taken place, i.e. within the last two weeks
bool isTwoWeeksAfterRace({clock = const Clock()}) {
  DateTime now = clock.now().toUtc();
  int year = now.year;
  DateTime rEnd = raceEnd(year);
  return isBetween(now, rEnd, rEnd.add(Duration(days: 14)));
}
