import 'package:flutter_cache_manager/flutter_cache_manager.dart';

const String BASE_URL_API_ISA =
    'https://www.spartathlon.gr/en/?option=com_ajax&plugin=spartathlon&format=json&action=';
const String BASE_URL_API_APP = 'https://spartathlon.app/api/';
const String API_URL_NEWS = BASE_URL_API_APP + 'news';
const String API_URL_EVENTS = BASE_URL_API_APP + 'timetable';

const _key = 'spartaCache';

/// Cache manager for all our network requests
/// We simply rely on the backend setting the Cache-Control headers properly
final CacheManager spartaCacheManager = CacheManager(Config(_key));
