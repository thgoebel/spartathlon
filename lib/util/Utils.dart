import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:platform/platform.dart';
import 'package:url_launcher/url_launcher.dart';

/// Corner radius for cards, dialogs, etc
const double kCardCornerRadius = 8.0;

/// Elevations of material cards
const double kCardElevation = 2.0;

/// Corner radius for Material buttons
const double kButtonCornerRadius = 4.0;

/// The height of a LicenseTile in the licenses dialog
const double kLicenseTileHeight = 72.0;

/// The height of the yellow notice bar warning about no network connection
const double kNoNetworkBarHeight = 40.0;

/// The default blur radius for shadows
const double kShadowBlurRadius = 4.0;

/// Return a string with a URL to open that location in a maps app.
///
/// The URI starts with geo:// on Android or with maps:// on iOS
/// or normal https:// to Google Maps on web.
String getGeoUrl(String lat, String long, Platform platform) {
  // TODO test me
  if (kIsWeb) {
    // This needs to come first, otherwise platform.isXYZ crashes on web.
    // https://github.com/flutter/flutter/issues/36126
    return 'https://www.google.com/maps/search/?api=1&query=' +
        lat +
        ',' +
        long;
  } else if (platform.isIOS) {
    return 'maps:' + lat + ',' + long + '?q=' + lat + ',' + long;
  } else if (platform.isAndroid) {
    return 'geo:' + lat + ',' + long + '?q=' + lat + ',' + long;
  } else {
    return 'https://www.google.com/maps/search/?api=1&query=' +
        lat +
        ',' +
        long;
  }
}

/// Tries to guess if the URL is relative.
/// If it is, adds the prefix "spartathlon.gr".
///
/// This is useful for URLs in blog posts that are relative.
String spartafyUrl(String? url) {
  // It's more convenient to handle the null here, rather than in all the onTap callbacks
  if (url == null) return '';

  if (url.indexOf('://') > 0 || url.startsWith('www')) {
    return url;
  }
  return 'https://www.spartathlon.gr' + url;
}

/// Launches an URL (like http, geo, etc) in any application the device provides
void launchUrl(String url) async {
  try {
    await launch(url);
  } catch (e) {
    // e.g. on Android 11: https://developer.android.com/training/basics/intents/package-visibility
    print('ERROR: Could not launch $url: $e');
  }
}

/// Returns true if the device is connected to any network
///
/// You can only call this on a real device, e.g. see 93d6537c
Future<bool> hasConnectivity() async {
  var connResult = await Connectivity().checkConnectivity();
  bool hasConn = (connResult == ConnectivityResult.none) ? false : true;
  return hasConn;
}

/// Return the title padding for the app bar title in an expanded app bar
EdgeInsets getPlatformDependentTitlePadding(bool isCollapsed) {
  if (LocalPlatform().isAndroid) {
    if (isCollapsed) {
      // align to the far left if flexible space is expanded
      return EdgeInsets.only(bottom: 16.0, left: 56.0);
    } else {
      return EdgeInsets.only(bottom: 16.0, left: 16.0);
    }
  } else if (LocalPlatform().isIOS) {
    // iOS centers the title
    return EdgeInsets.only(bottom: 16.0, left: 0.0);
  } else {
    return EdgeInsets.only(bottom: 16.0, left: 0.0);
  }
}

/// Returns true if DateTime [a] and DateTime [b] are on the same day
bool sameDay(DateTime a, DateTime b) {
  return a.year == b.year && a.month == b.month && a.day == b.day;
}