/// Licenses of art / libraries used in JSON format

// Order: This app, official dart, official flutter, others
// Else in lexicographic order
const licenses = [
  {
    'name': 'Spartathlon App',
    'author': 'Thore Goebel',
    'license': 'GPLv3 License',
    'url': 'https://gitlab.com/thgoebel/spartathlon/blob/main/LICENSE'
  },
  {
    'name': 'Clock',
    'author': 'Dart Team',
    'license': 'Apache 2.0 License',
    'url': 'https://pub.dev/packages'
  },
  {
    'name': 'http',
    'author': 'Dart Team',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/http'
  },
  {
    'name': 'Connectivity',
    'author': 'Flutter Team',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/connectivity'
  },
  {
    'name': 'Cupertino Icons',
    'author': 'Flutter Team',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/cupertino_icons'
  },
  {
    'name': 'Path Provider',
    'author': 'Flutter Team',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/path_provider'
  },
  {
    'name': 'Shared Preferences',
    'author': 'Flutter Team',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/shared_preferences'
  },
  {
    'name': 'URL Launcher',
    'author': 'Flutter Team',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/url_launcher'
  },
  {
    'name': 'WebView',
    'author': 'Flutter Team',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/webview_flutter'
  },
  {
    'name': 'Tuple',
    'author': 'Google',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/tuple'
  },
  {
    'name': 'Cached Network Image',
    'author': 'Rene Floor',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/cached_network_image'
  },
  {
    'name': 'csv',
    'author': 'Christian Loitsch',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/csv'
  },
  {
    'name': 'Flutter Cache Manager',
    'author': 'Rene Floor',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/flutter_cache_manager'
  },
  {
    'name': 'Flutter HTML',
    'author': 'Matthew Whitaker',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/flutter_html'
  },
  {
    'name': 'Flutter Launcher Icons',
    'author': 'Flutter Community',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/flutter_launcher_icons'
  },
  {
    'name': 'Flutter Sticky Headers',
    'author': 'Romain Rastel',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/flutter_sticky_header'
  },
  {
    'name': 'Platform',
    'author': 'Todd Volkert',
    'license': 'BSD License',
    'url': 'https://pub.dev/packages/platform'
  },
  {
    'name': 'Provider',
    'author': 'dash-overflow.net',
    'license': 'MIT License',
    'url': 'https://pub.dev/packages/provider'
  },
];
