import 'dart:convert';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:spartathlon_app/models/Event.dart';
import 'package:spartathlon_app/util/ApiCache.dart';
import 'package:spartathlon_app/util/RaceTiming.dart';
import 'package:spartathlon_app/util/Utils.dart';

class EventProvider {
  /// Fetches the list of events from the API
  static Future<List<Event>> getEvents(
    CacheManager cacheManager, {
    bool forceReload = false,
  }) async {
    print('Getting events...');
    List<Event> events = [];

    // Force refresh the events (to ensure up-to-date-ness in case of changes)
    if (forceReload || isRaceWeek()) {
      print('Forcefully re-downloading events');
      await cacheManager.downloadFile(API_URL_EVENTS);
    }
    var apiResponse = await cacheManager.getSingleFile(API_URL_EVENTS);

    // No network connection to the API, just return an empty list
    // TODO test
    //if (apiResponse == null) {
     // return List<Event>();
    //}

    List eventsAsJson = json.decode(await apiResponse.readAsString()) as List;
    for (var e in eventsAsJson) {
      events.add(Event.fromJson(e));
    }
    events.sort();
    print('Finished loading events');
    return events;
  }

  /// Returns true if any event in [events] happens today or in the future
  static bool hasCurrentOrFutureEvents(List<Event> events, DateTime now) {
    for (Event e in events) {
      if (e.isOnSameDayOrLater(now)) {
        return true;
      }
    }
    return false;
  }

  /// Return a list created from [events] made up of lists all containing the
  /// events of the same day, ordered by the day.
  /// Excludes all past before [now].
  static List<List<Event>> getEventsPerDay(List<Event> events, DateTime now) {
    // Use a local copy since we are removing events while iterating
    List<Event> eventsAll = List<Event>.from(events);
    List<List<Event>> eventsPerDay = [];
    eventsAll.sort();

    while (eventsAll.isNotEmpty) {
      Event next = eventsAll.first;
      List<Event> eventsOnThisDay = [];
      for (Event e in eventsAll) {
        if (sameDay(e.from, next.from)) {
          eventsOnThisDay.add(e);
        }
      }

      // Only include future events
      if (next.isOnSameDayOrLater(now)) {
        eventsPerDay.add(eventsOnThisDay);
      }
      eventsAll.removeWhere((Event e) => sameDay(e.from, next.from));
    }
    return eventsPerDay;
  }
}
