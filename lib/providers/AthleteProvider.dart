import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/util/ApiCache.dart';
import 'package:spartathlon_app/util/RaceTiming.dart';

/// Provides list of athletes.
///
/// Currently, these are only the athletes who compete(d) in this year's race (since
/// this is all the API offers).

class AthleteProvider {
  static const String SETTING_FAV_ATHLETES = 'favoriteAthletesIds';

  /// Fetches the list of athletes via the Spartathlon's API.
  ///
  /// N.B: Only athletes who competed last year or will compete this
  /// year are included, and only those.
  /// This is an issue that should preferably be fixed server side.
  static Future<List<Athlete>> getAthletes(bool forceReload) async {
    print("Getting athletes");
    List<Athlete> athletes = [];

    // TODO Bring back athletes using our own backend.
    // TODO Revisit nullability of Athlete's fields
    // ignore_for_file: dead_code
    return athletes;

    // Force refresh the athlete list if the race is on and we have a connection
    // We do this because this list contains the live results, and we want to make
    // sure they are up-to-date
    if (forceReload || isRaceOn()) {
      print('Forcefully re-downloading athletes...');
      await spartaCacheManager.downloadFile(BASE_URL_API_ISA + 'getAthletes');
    }
    var apiResponse = await spartaCacheManager
        .getSingleFile(BASE_URL_API_ISA + 'getAthletes');

    List athletesAsJson =
        json.decode(await apiResponse.readAsString())['message'] as List;
    for (var e in athletesAsJson) {
      athletes.add(Athlete.fromJson(e));
    }
    athletes.sort();
    print("Finished loading athletes");
    return athletes;
  }

  /// For a given athlete object only containing the athlete ID, this method
  /// fetches the remaining fields (date of birth, biography, finishes)
  static Future<Athlete> getFullAthleteDetails(Athlete a) async {
    print("Getting full athlete details for athlete " + a.name);

    String url = BASE_URL_API_ISA + 'getAthlete&athleteID=' + a.id.toString();
    var apiResponse = await spartaCacheManager.getSingleFile(url);

    Map<String, dynamic> js =
        json.decode(await apiResponse.readAsString())['message']
            as Map<String, dynamic>;

    // Add missing values to fields. These are not in the API response for
    // getAthletes and need to be lazily filled with individual per-athlete
    // requests.
    // For nation field, see https://gitlab.com/thgoebel/spartathlon/issues/17
    a.nation = js['nationality'];
    a.dateOfBirth = js['birthdate'];
    a.biography = js['minicv'];
    List<Finish> finishes = [];
    for (var i in js['timings']) {
      Finish f = Finish(
        year: i['year'],
        bib: i['number'],
        time: i['total_time'],
      );
      finishes.add(f);
    }
    a.finishes = finishes;
    a.isFullyLoaded = true;

    return a;
  }

  /// Returns a list of athletes objects of all athletes that the user has marked
  /// as favorite.
  static Future<List<Athlete>> getFavoriteAthletes(
      List<Athlete> athletes) async {
    print("Getting favorite athletes");

    List<String> favIds = await getFavoriteAthleteIds();
    List<Athlete> favAthletes = [];

    for (Athlete a in athletes) {
      if (favIds.contains(a.id)) {
        favAthletes.add(a);
      }
    }
    print("Finished loading favorite athletes");
    return favAthletes;
  }

  /// Returns a list of strings that contains the ids of all athletes that the
  /// user has marked as favorite.
  /// The list is saved on disk in shared preferences.
  static Future<List<String>> getFavoriteAthleteIds() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> favIds = prefs.getStringList(SETTING_FAV_ATHLETES) ?? [];
    return favIds;
  }

  /// Adds the id to the list of favorite athletes
  static setToggleAthleteFavoriteToPrefs(String id) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> favIds = prefs.getStringList(SETTING_FAV_ATHLETES) ?? [];

    if (favIds.contains(id)) {
      favIds.remove(id);
    } else {
      favIds.add(id);
    }
    await prefs.setStringList(SETTING_FAV_ATHLETES, favIds);
    // Ignore failure to save, e.g. on web
  }

  static Athlete? getAthleteById(List<Athlete> athletes, String id) {
    for (Athlete a in athletes) {
      if (a.id.compareTo(id) == 0) return a;
    }
    return null;
  }
}
