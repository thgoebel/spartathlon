import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeProvider extends ChangeNotifier {
  static const String SETTING_THEME = 'theme';

  /// Asynchronously returns the id of the ThemeMode currently stored in the shared prefs.
  /// We assume that this is consistent with the one Flutter currently uses to draw the widgets (this should be made sure elsewhere!).
  static Future<ThemeMode> getThemeModeFromPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int modeId = prefs.getInt(SETTING_THEME) ?? ThemeMode.system.index;
    return ThemeMode.values[modeId];
  }

  /// Stores new app theme in shared preferences to persist across restarts.
  static setThemeModeToPrefs(ThemeMode mode) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('Setting app theme to $mode');
    await prefs.setInt(SETTING_THEME, mode.index);
    // Ignore failure to save, e.g. on web
  }
}
