import 'dart:async';
import 'dart:convert';

import 'package:spartathlon_app/models/BlogPost.dart';
import 'package:spartathlon_app/util/ApiCache.dart';
import 'package:spartathlon_app/util/RaceTiming.dart';

/// Provides a list of blog entries (race reports and press releases).
class BlogProvider {
  /// Fetches the list of events from the API
  static Future<List<BlogPost>> getBlogPosts(bool forceReload) async {
    List<BlogPost> blogPosts = <BlogPost>[];
    print('Getting blog posts...');

    if (forceReload || isTwoWeeksAfterRace()) {
      print('Forcefully re-downloading blog posts...');
      await spartaCacheManager.downloadFile(API_URL_NEWS);
    }
    var apiResponse = await spartaCacheManager.getSingleFile(API_URL_NEWS);

    // No network connection to the API, just return an empty list
    // TODO check what happens with networking
   // if (apiResponse == null) {
   //   return List<BlogPost>();
    //}

    List postsAsJson = json.decode(await apiResponse.readAsString()) as List;
    for (var e in postsAsJson) {
      blogPosts.add(BlogPost.fromJson(e));
    }
    blogPosts.sort();

    print("Finished loading blog posts");
    return blogPosts;
  }
}
