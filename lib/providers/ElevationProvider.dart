import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:spartathlon_app/views/elevation/ElevationView.dart';
import 'package:tuple/tuple.dart';

// Indices of the columns in the route.csv
const int colLat = 0;
const int colLong = 1;
const int colEle = 2;
const int colDist = 3;

typedef RouteCsv = List<List>;

/// Provides elevation-drawing related functionality
class ElevationProvider {

  static RouteCsv? _routeCsv;

  /// Reads the CSV asset containing GPS data points with the elevation at
  /// each distance of the race.
  static Future<RouteCsv> getElevationCsv(BuildContext context) async {
    if (_routeCsv == null) {
      await _loadCsvFromAssets(context);
    }
    return _routeCsv!;
  }

  // Expensive: opens a file AND parses 2500 rows!
  static Future _loadCsvFromAssets(BuildContext context) async {
    print('Loading route.csv'); // should only be loaded once

    RouteCsv csv = await DefaultAssetBundle.of(context)
        .loadString("assets/route.csv", cache: true)
        .then((csvAsString) => CsvToListConverter().convert(csvAsString));

    csv.removeAt(0); // remove header row
    _routeCsv = List.unmodifiable(csv);
  }

  /// Returns the elevation in meter at [distance] in the race.
  ///
  /// The result is an approximation, namely the average elevation of the
  /// GPS data points that are just before and after the given [distance].
  ///
  /// This function assumes that [profile] is of the correct CSV shape.
  static double getElevationAtDistance(List<List> profile, double distance) {
    assert(distance >= DISTANCE_ATHENS && distance <= DISTANCE_SPARTA);

    for (int i = 1; i < profile.length; i++) {
      if (profile[i][colDist] > distance) {
        // The values in the CSV may both be ints!
        double sum = (profile[i - 1][colEle] + profile[i][colEle]).toDouble();
        return sum / 2;
      }
    }
    return profile.last[colEle];
  }

  /// Returns a tuple where the first and second elements are the minimum and
  /// maximum elevation value in [profile] respectively.
  ///
  /// This function assumes that [profile] is of the correct CSV shape.
  static Tuple2 getMinMaxElevation(List<List<dynamic>> profile) {
    double min = double.infinity;
    double max = double.negativeInfinity;

    for (List row in profile) {
      if (row[colEle] < min) {
        min = row[colEle];
      }
      if (row[colEle] > max) {
        max = row[colEle];
      }
    }
    return Tuple2(min, max);
  }
}
