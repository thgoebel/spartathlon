import 'package:spartathlon_app/models/Cp.dart';
import 'package:spartathlon_app/providers/CpData.dart';

/// Provides list of CPs
///
/// To that end, it parses the JSON in CpData.dart and builds Cp objects from it.

class CpProvider {
  /// Returns Spartathlon's 76 checkpoints
  static List<Cp> getCps() {
    return checkpoints
        .map((Map<String, Object> e) => Cp(
              id: int.parse(e['id'].toString()),
              name: e['name'].toString(),
              fromStart: double.parse(e['fromStart'].toString()),
              toNext: double.parse(e['toNext'].toString()),
              toFinish: double.parse(e['toFinish'].toString()),
              openingTime: e['openingTime'].toString().substring(0, 5),
              closingTime: e['closingTime'].toString().substring(0, 5),
              liveTiming: e['liveTiming'] == 'yes',
              supporterAllowed: (e['supporterAllowed'] == 'yes'),
              firstAid: e['firstAid'] == 'yes',
              isMajor: e['isMajor'] == 'yes',
              isTown: e['isTown'] == 'yes',
              long: double.parse(e['long'].toString()),
              lat: double.parse(e['lat'].toString()),
              supplies: e['supplies'].toString(),
              description: e['description'].toString(),
            ))
        .toList();
  }

  /// Returns a list of all "major" checkpoints.
  ///
  /// See [Cp] for our definition of "major".
  static List<Cp> getMajorCps() {
    return getCps().where((cp) => cp.isMajor).toList();
  }

  /// Returns a list of all "minor" checkpoints, i.e. those that are NOT "major".
  static List<Cp> getMinorCps() {
    return getCps().where((cp) => !cp.isMajor).toList();
  }

  /// Returns the "general" name of the place where the checkpoint is located
  /// using a lookup in a hardcoded map.
  // TODO Move to backend
  static String getCpLocation(int cpId) {
    // Note: 13 characters max, anything else will be cut off until we dynamically
    // resize the FavAthleteCard
    Map<int, String?> locationMap = {
      0: 'Athens',
      4: 'Aspropyrgos',
      11: 'Megara',
      22: 'Corinth',
      35: 'Ancient Nemea',
      43: 'Lyrkia',
      47: 'Mountain Base',
      52: 'Nestani',
      60: 'Alea Tegea',
      69: null, // End of a steep ascent, hurts a lot
      75: 'Sparta',
    };

    String? s = locationMap[cpId];
    if (s == null) {
      return '';
    } else {
      return ' (' + s + ')';
    }
  }
}
