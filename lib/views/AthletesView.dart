import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/AppStateContainer.dart';
import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/views/AthleteDetailsView.dart';
import 'package:spartathlon_app/views/general/ErrorView.dart';
import 'package:spartathlon_app/views/general/LoadingView.dart';

/// Athletes View
///
/// Displays all the athletes who compete(d) in this year's race. Features a search
/// bar on top to find athletes by name or country

class AthletesView extends StatefulWidget {
  AthletesView({Key? key}) : super(key: key);

  @override
  _AthletesViewState createState() => _AthletesViewState();
}

class _AthletesViewState extends State<AthletesView> {
  final TextEditingController searchController = TextEditingController();

  // nullable until it is initialised in the first build()
  List<Athlete>? athletesToDisplay;

  @override
  Widget build(BuildContext context) {
    // Clone list since we want to edit it
    if (athletesToDisplay == null) {
      athletesToDisplay =
          List<Athlete>.from(AppStateContainer.of(context).data.athletes);
    }

    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(left: 16.0, right: 0.0, top: 2.0),
          height: 50.0,
          decoration: BoxDecoration(
            border: Border(
              bottom:
                  BorderSide(color: Theme.of(context).dividerColor, width: 1.5),
            ),
          ),
          child: Row(
            children: [
              Icon(
                Icons.search,
                color: Colors.grey,
              ),
              Container(
                width: MediaQuery.of(context).size.width - 95,
                child: TextField(
                  onChanged: (s) {
                    _onSearch(s.toString());
                  },
                  controller: searchController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.only(left: 16.0, top: 12.0, bottom: 12.0),
                    filled: false,
                    hintText: SpartaLocalizations.of(context).hintSearchBar,
                    hintStyle: TextStyle(color: Colors.grey),
                  ),
                ),
              ),
              searchController.text.isNotEmpty
                  // Only show x to clear search if there is some search term
                  ? IconButton(
                      icon: Icon(
                        Icons.clear,
                        color: Colors.grey,
                      ),
                      tooltip:
                          SpartaLocalizations.of(context).tooltipClearSearch,
                      onPressed: () {
                        searchController.clear();
                        // https://stackoverflow.com/a/44991969/1068346
                        FocusScope.of(context).requestFocus(new FocusNode());
                        _onSearch('');
                      },
                    )
                  : Container(),
            ],
          ),
        ),
        Expanded(
          child: _getBody(),
        ),
      ],
    );
  }

  Widget _getBody() {
    AppStateContainerState container = AppStateContainer.of(context);
    if (container.data.isLoading) {
      return LoadingView();
    }
    if (container.data.hasError) {
      return ErrorView(
        onRetry: AppStateContainer.of(context).reloadAfterError,
        text: SpartaLocalizations.of(context).errorNotLoaded,
      );
    }

    if (athletesToDisplay!.isNotEmpty) {
      return RefreshIndicator(
        onRefresh: () => container.loadData(true),
        child: Scrollbar(
          child: ListView.builder(
              itemCount: athletesToDisplay!.length,
              itemBuilder: (BuildContext context, int position) =>
                  _getAthleteTile(position)),
        ),
      );
    } else {
      return Center(
        child: Text(SpartaLocalizations.of(context).errorNoAthleteSearchResult),
      );
    }
  }

  /// Returns a ListTile with the athlete avatar and his/her name
  Widget _getAthleteTile(int i) {
    assert(i < athletesToDisplay!.length);
    Athlete a = athletesToDisplay![i];

    return ListTile(
      leading: _getAvatar(a),
      trailing: FutureBuilder<bool>(
          future: a.isFavorite(),
          builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data == true) {
                return IconButton(
                    icon: Icon(
                      Icons.star,
                      color: Color(0xFFFFC629), // bright yellow
                    ),
                    onPressed: () {
                      setState(() {
                        AppStateContainer.of(context)
                            .setToggleFavoriteAthlete(a.id);
                      });
                    });
              } else {
                // Button should be there, but not visible
                return Container(
                  width: 50.0,
                  child: MaterialButton(onPressed: () {
                    setState(() {
                      AppStateContainer.of(context)
                          .setToggleFavoriteAthlete(a.id);
                    });
                  }),
                );
              }
            } else {
              return Container(
                height: 1.0,
                width: 1.0,
              );
            }
          }),
      title: Text(a.name),
      onTap: () {
        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: (context) => AthleteDetailsView(athlete: a),
          ),
        );
      },
    );
  }

  /// Returns CircleAvatar with either an image or the runner's initials
  CircleAvatar _getAvatar(Athlete a) {
    return CircleAvatar(
      backgroundColor: Theme.of(context).accentColor,
      backgroundImage: CachedNetworkImageProvider(a.imageUrl),
    );
  }

  /// Sets the state variable 'athletesToDisplay' to a new list containing only
  /// those athletes whose name INCLUSIVE OR country OR bib number contains the
  /// input 'text'.
  /// If 'text' is empty, the full list of athlete is returned.
  _onSearch(String text) {
    AppStateContainerState container = AppStateContainer.of(context);
    List<Athlete> newToDisplay;

    if (text.isEmpty) {
      newToDisplay = List<Athlete>.from(container.data.athletes);
    } else {
      newToDisplay = <Athlete>[];

      int textAsInt = -1;
      try {
        textAsInt = int.parse(text);
      } catch (_) {}

      // Search for name, country or bib number
      for (Athlete a in container.data.athletes) {
        if (a.name.toLowerCase().contains(text.toLowerCase()) ||
            a.nation.toLowerCase().contains(text.toLowerCase()) ||
            a.bibNumber == textAsInt) {
          newToDisplay.add(a);
        }
      }
    }
    athletesToDisplay!.sort();
    setState(() {
      athletesToDisplay = newToDisplay;
    });
  }

  @override
  void dispose() {
    // Clean up controller when widget is disposed
    searchController.dispose();
    super.dispose();
  }
}
