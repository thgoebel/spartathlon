import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';

class LoadingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Center(
        child: Container(
          //constraints: BoxConstraints.tight(Size.fromHeight(120.0)),
          padding: EdgeInsets.all(32.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                  padding: EdgeInsets.only(bottom: 24.0),
                  child: CircularProgressIndicator()),
              Text(SpartaLocalizations.of(context).textLoadingView),
            ],
          ),
        ),
      ),
    );
  }
}
