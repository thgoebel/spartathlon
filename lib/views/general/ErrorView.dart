import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/util/Utils.dart';

class ErrorView extends StatelessWidget {
  final VoidCallback onRetry;
  final String text;

  const ErrorView({
    Key? key,
    required this.onRetry,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: Center(
          child: Container(
            constraints: BoxConstraints.tight(Size.fromHeight(120.0)),
            padding: EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(text),
                MaterialButton(
                  color: Theme.of(context).primaryColor,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(kButtonCornerRadius),
                  ),
                  onPressed: onRetry,
                  child: Text(SpartaLocalizations.of(context).labelButtonRetry),
                ),
              ],
            ),
          ),
        ));
  }
}
