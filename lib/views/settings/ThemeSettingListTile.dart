import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:provider/provider.dart';
import 'package:spartathlon_app/models/ThemeModeChangeNotifier.dart';
import 'package:spartathlon_app/util/Themes.dart';

class ThemeSettingListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeModeChangeNotifier>(
      builder: (context, themeModeChangeNotifier, child) {
        final mode = themeModeChangeNotifier.themeMode;

        return ListTile(
          leading: Icon(
            Icons.palette_outlined,
            color: Theme.of(context).primaryColor,
          ),
          title: Text(SpartaLocalizations.of(context).labelSettingTheme),
          trailing: Padding(
            padding: EdgeInsets.only(right: 12.0),
            child: Text(themeModeToString(context, mode)),
          ),
          onTap: () {
            showDialog(
                context: context,
                builder: (context) {
                  return SimpleDialog(
                    title: Text(
                        SpartaLocalizations.of(context).titlePopupThemeChooser),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    children: [
                      RadioListTile<ThemeMode>(
                        title:
                            Text(themeModeToString(context, ThemeMode.system)),
                        value: ThemeMode.system,
                        groupValue: mode,
                        onChanged: (ThemeMode? newMode) =>
                            _onSaveThemeMode(context, newMode),
                      ),
                      RadioListTile<ThemeMode>(
                        title:
                            Text(themeModeToString(context, ThemeMode.light)),
                        value: ThemeMode.light,
                        groupValue: mode,
                        onChanged: (ThemeMode? newMode) =>
                            _onSaveThemeMode(context, newMode),
                      ),
                      RadioListTile<ThemeMode>(
                        title: Text(themeModeToString(context, ThemeMode.dark)),
                        value: ThemeMode.dark,
                        groupValue: mode,
                        onChanged: (ThemeMode? newMode) =>
                            _onSaveThemeMode(context, newMode),
                      ),
                    ],
                  );
                });
          },
        );
      },
    );
  }

  void _onSaveThemeMode(BuildContext context, ThemeMode? newMode) {
    if (newMode == null) return;

    Provider.of<ThemeModeChangeNotifier>(context, listen: false).themeMode =
        newMode;

    // Close dialog
    Navigator.pop(context, true);
  }
}
