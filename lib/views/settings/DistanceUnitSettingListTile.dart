import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:provider/provider.dart';
import 'package:spartathlon_app/models/DistanceUnitChangeNotifier.dart';
import 'package:spartathlon_app/util/DistanceUnit.dart';

class DistanceUnitSettingListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<DistanceUnitChangeNotifier>(
      builder: (context, distanceUnitChangeNotifier, child) {
        DistanceUnit currentUnit = distanceUnitChangeNotifier.distanceUnit;
        String unitString = distanceUnitToString(context, currentUnit);

        return ListTile(
          leading: Icon(
            Icons.redo,
            color: Theme.of(context).primaryColor,
          ),
          title: Text(SpartaLocalizations.of(context).labelSettingDistanceUnit),
          trailing: Padding(
            padding: EdgeInsets.only(right: 12.0),
            child: Text(unitString),
          ),
          onTap: () {
            showDialog(
                context: context,
                builder: (context) {
                  return SimpleDialog(
                    title: Text(
                      SpartaLocalizations.of(context)
                          .titlePopupDistanceUnitChooser,
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    children: [
                      RadioListTile<DistanceUnit>(
                        title: Text(
                          SpartaLocalizations.of(context).distanceUnitKm,
                        ),
                        value: DistanceUnit.km,
                        groupValue: currentUnit,
                        onChanged: (newUnit) =>
                            _onSaveDistanceUnit(context, newUnit),
                      ),
                      RadioListTile<DistanceUnit>(
                        title: Text(
                          SpartaLocalizations.of(context).distanceUnitMiles,
                        ),
                        value: DistanceUnit.miles,
                        groupValue: currentUnit,
                        onChanged: (newUnit) =>
                            _onSaveDistanceUnit(context, newUnit),
                      ),
                    ],
                  );
                });
          },
        );
      },
    );
  }

  void _onSaveDistanceUnit(BuildContext context, DistanceUnit? newUnit) {
    if (newUnit == null) return;

    Provider.of<DistanceUnitChangeNotifier>(context, listen: false)
        .distanceUnit = newUnit;

    Navigator.pop(context, true);
  }
}

String capitalize(String s) {
  if (s.isEmpty) {
    return '';
  }
  return s[0].toUpperCase() + s.substring(1);
}
