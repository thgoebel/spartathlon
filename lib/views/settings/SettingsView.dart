import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/views/AboutView.dart';
import 'package:spartathlon_app/views/settings/ClearCacheSettingListTile.dart';
import 'package:spartathlon_app/views/settings/DistanceUnitSettingListTile.dart';
import 'package:spartathlon_app/views/settings/ThemeSettingListTile.dart';

/// Settings view

class SettingsView extends StatefulWidget {
  SettingsView({Key? key}) : super(key: key);

  @override
  _SettingsViewState createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(SpartaLocalizations.of(context).appBarTitleSettings),
      ),
      body: Column(
        children: _getSettingsList(context),
      ),
    );
  }

  /// Returns a list of widgets that make up the settings screen
  List<Widget> _getSettingsList(BuildContext context) {
    return [
      ThemeSettingListTile(),
      DistanceUnitSettingListTile(),
      // Should NOT be shown to production users
      if (kDebugMode) ClearCacheSettingListTile(),
      // ABOUT
      ListTile(
        title: Text(SpartaLocalizations.of(context).appBarTitleAbout),
        leading: Icon(
          Icons.info_outline,
          color: Theme.of(context).primaryColor,
        ),
        onTap: () {
          Navigator.push(
              context, CupertinoPageRoute(builder: (context) => AboutView()));
        },
      ),
    ];
  }
}
