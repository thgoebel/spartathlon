import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/AppStateContainer.dart';

class ClearCacheSettingListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: Icon(
          Icons.delete_outline,
          color: Theme.of(context).primaryColor,
        ),
      ),
      title: Text(SpartaLocalizations.of(context).labelSettingClearCache),
      onTap: () {
        AppStateContainer.of(context).evictCache();

        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content:
                Text(SpartaLocalizations.of(context).snackbarTextCacheCleared),
          ),
        );
      },
    );
  }
}
