import 'package:spartathlon_app/models/Cp.dart';

/// Class used to cache a CP and its location on a canvas
class PaintedCp {
  PaintedCp({
    required this.cp,
    required this.dx,
    required this.dy,
  });

  /// "Radius" of the square area around the painted CP inside which taps are
  /// recognised as belonging to this CP
  static const int TOUCH_AREA_RADIUS = 25;

  /// Checkpoint that is painted on the canvas
  Cp cp;

  /// Coordinates of the checkpoint on the canvas.
  /// Note that they denote the CENTER point of the drawn CP, NOT its top left!
  double dx, dy;

  /// Returns true if the point ([hitX], [hitY]) is within the touch area of this
  /// [PaintedCp].
  bool isHitBy(double hitX, hitY) {
    bool inXRange = hitX >= dx - TOUCH_AREA_RADIUS &&
        hitX <= dx + TOUCH_AREA_RADIUS;
    bool inYRange = hitY >= dy - TOUCH_AREA_RADIUS &&
        hitY <= dy + TOUCH_AREA_RADIUS;
    return inXRange && inYRange;
  }

  @override
  String toString() => "PaintedCp for CP$cp.id";
}
