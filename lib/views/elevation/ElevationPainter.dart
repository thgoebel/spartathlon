import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:spartathlon_app/models/Cp.dart';
import 'package:spartathlon_app/providers/CpProvider.dart';
import 'package:spartathlon_app/providers/ElevationProvider.dart';
import 'package:spartathlon_app/util/DistanceUnit.dart';
import 'package:spartathlon_app/views/elevation/PaintedCp.dart';
import 'package:tuple/tuple.dart';

/// Width of the elevation profile
const double LINE_WIDTH = 3;

/// Radius of the circles marking the CPs on the profile
const double CP_MARKER_RADIUS = 5;

/// Offsets for the axis labels
const double X_AXIS_BOTTOM_OFFSET = 25;
const double Y_AXIS_LEFT_OFFSET = 10;

/// Padding that is reserved for the x-axis
/// The y-axis currently has - by design - no padding, i.e. the profile is allowed
/// to draw over it.
/// This is intentional, in order to make the most of the limited screen width.
const double PROFILE_BOTTOM_PADDING = 30;

const double PROFILE_TOP_PADDING = 10;

/// Hardcoded labels for the y-axis, meaning elevation in meters
const List<int> Y_AXIS_LABELS = [200, 400, 600, 800, 1000];

/// Visible range in km below which minor CP **markers** will be drawn
const int THRESHOLD_SHOW_MINOR_CPS = 50;

/// Visible range in km below which minor CP **details** (distance, town names) will be drawn
const int THRESHOLD_LABEL_MINOR_CPS = 30;

/// Visible range above which points on the graph will be dropped to prevent slow drawing
const int THRESHOLD_DROP_POINTS = 100;

/// Paints the elevation profile
class ElevationPainter extends CustomPainter {
  ElevationPainter({
    required this.profile,
    required this.unit,
    required this.from,
    required this.to,
    required this.markerColor,
    required this.markerColorLight,
    required this.elevationLineColor,
  }) {
    Tuple2 minMax = ElevationProvider.getMinMaxElevation(profile);
    this.minEle = minMax.item1;
    this.maxEle = minMax.item2;
  }

  RouteCsv profile;

  /// Distance unit to show
  DistanceUnit unit;

  /// Distance on the race course that should be at the LEFT edge of the canvas
  double from;

  /// Distance on the race course that should be at the RIGHT edge of the canvas
  double to;

  /// Minimum elevation on the entire course
  late double minEle;

  /// Maximum elevation on the entire course
  late double maxEle;

  /// Color to mark CPs and axis in
  Color markerColor;
  Color markerColorLight;
  Color elevationLineColor;

  static const double textSize = 14;
  static const double textMaxWidth = 300; // Educated guess

  @override
  void paint(Canvas canvas, Size size) {
    print("Painting elevation profile...");

    /* Draw elevation profile */
    _paintProfileLine(canvas, size);

    /* Draw y-axis (elevation) */
    _paintYAxis(canvas, size);

    /* Draw CPs (include x-axis) */
    _paintMajorCps(canvas, size);
    _paintMinorCps(canvas, size);

    /* Draw position of favourite athletes */
    // TODO
  }

  @override
  bool shouldRepaint(ElevationPainter oldDelegate) {
    // Repaint if either of the two bounds has changed
    bool needsRepaint = from != oldDelegate.from || to != oldDelegate.to;
    //print(
    //    "Repainting $needsRepaint: from(${oldDelegate.from} -> $from) to(${oldDelegate.to} -> $to)");
    return needsRepaint;
  }

  /// Paints the elevation profile onto a canvas
  void _paintProfileLine(Canvas canvas, Size size) {
    Paint elevationLinePaint = Paint()
      ..color = elevationLineColor
      ..strokeWidth = LINE_WIDTH;

    List<List> profileToDraw = getPointsInRange(profile, from, to);

    List<Offset> points = [];
    for (List row in profileToDraw) {
      Offset offset = Offset(
        _dx(size.width, row[colDist].toDouble()),
        _dy(size.height, row[colEle].toDouble()),
      );
      points.add(offset);
    }
    canvas.drawPoints(ui.PointMode.polygon, points, elevationLinePaint);
  }

  /// Paints the y-axis (elevation in meters) using a hardcoded list of labels
  void _paintYAxis(Canvas canvas, Size size) {
    for (int label in Y_AXIS_LABELS) {
      double labelDy = _dy(size.height, label.toDouble());

      ui.ParagraphBuilder labelBuilder = ui.ParagraphBuilder(
          ui.ParagraphStyle(fontSize: textSize, textAlign: TextAlign.left))
        ..pushStyle(ui.TextStyle(color: markerColor))
        ..addText(label.toString());

      ui.Paragraph labelParagraph = labelBuilder.build()
        ..layout(ui.ParagraphConstraints(width: textMaxWidth));

      Offset labelOffset = Offset(Y_AXIS_LEFT_OFFSET, labelDy);
      canvas.drawParagraph(labelParagraph, labelOffset);
    }
  }

  /// Paints major CPs onto the canvas.
  ///
  /// These also have their distances marked as the x-axis.
  void _paintMajorCps(Canvas canvas, Size size) {
    List<Cp> cps = CpProvider.getMajorCps();
    List<PaintedCp> cpsToPaint =
        getPaintedCps(profile, cps, from, to, maxEle, size);

    Paint markerPaint = Paint()..color = markerColor;

    for (PaintedCp cp in cpsToPaint) {
      // Draw CP marker onto the profile
      Offset markerOffset = Offset(cp.dx, cp.dy);
      canvas.drawCircle(markerOffset, CP_MARKER_RADIUS, markerPaint);

      _drawCpName(canvas, size, cp);
      _drawCpDistance(canvas, size, cp);
    }
  }

  /// Paints minor CPs onto the canvas.
  ///
  /// These are only shown if the visible range is small enough, to prevent overcrowding the screen.
  void _paintMinorCps(Canvas canvas, Size size) {
    double visibleRange = to - from;

    List<Cp> cps = CpProvider.getMinorCps();
    List<PaintedCp> cpsToPaint =
        getPaintedCps(profile, cps, from, to, maxEle, size);

    for (PaintedCp cp in cpsToPaint) {
      if (visibleRange <= THRESHOLD_SHOW_MINOR_CPS) {
        Paint markerPaint = Paint()..color = markerColor;
        Offset markerOffset = Offset(cp.dx, cp.dy);
        canvas.drawCircle(markerOffset, CP_MARKER_RADIUS, markerPaint);
      }

      if (visibleRange <= THRESHOLD_LABEL_MINOR_CPS) {
        _drawCpDistance(canvas, size, cp);
        _drawCpName(canvas, size, cp);
      }
    }
  }

  /// Draws the number of the CP above the marker
  void _drawCpName(Canvas canvas, Size size, PaintedCp cp) {
    ui.TextStyle textStyle =
        ui.TextStyle(color: cp.cp.isTown ? markerColor : markerColorLight);

    ui.ParagraphBuilder nameBuilder = ui.ParagraphBuilder(
        ui.ParagraphStyle(fontSize: textSize, textAlign: TextAlign.center))
      ..pushStyle(textStyle)
      ..addText(cp.cp.profileLabel);

    ui.Paragraph nameParagraph = nameBuilder.build()
      ..layout(ui.ParagraphConstraints(width: textMaxWidth));

    double offsetFactorToTheTop = 1.75; // Educated guess
    Offset nameOffset = Offset(
      cp.dx - nameParagraph.width / 2,
      cp.dy - offsetFactorToTheTop * textSize,
    );

    canvas.drawParagraph(nameParagraph, nameOffset);
  }

  /// Draws the distance of the CP onto the x axis
  void _drawCpDistance(Canvas canvas, Size size, PaintedCp cp) {
    ui.ParagraphBuilder distanceBuilder = ui.ParagraphBuilder(
        ui.ParagraphStyle(fontSize: textSize, textAlign: TextAlign.center))
      ..pushStyle(ui.TextStyle(color: markerColor))
      ..addText(cp.cp.fromStart.toUnit(unit).round().toString());

    ui.Paragraph distanceParagraph = distanceBuilder.build()
      ..layout(ui.ParagraphConstraints(width: textMaxWidth));

    Offset distanceOffset = Offset(
      cp.dx - distanceParagraph.width / 2,
      size.height - X_AXIS_BOTTOM_OFFSET,
    );
    canvas.drawParagraph(distanceParagraph, distanceOffset);
  }

  /// Given a list of any CPs prefills their coordinates at the correct scale
  /// for the viewport indicated by [size].
  ///
  /// It does NOT filter on the visible range.
  /// Since there are only of them, there overhead of drawing to many should be acceptable.
  ///
  /// This method is static so that we can also use it to resolve tapped checkpoints
  /// without an ElevationPainter object.
  static List<PaintedCp> getPaintedCps(
      List<List> profile, List<Cp> cps, double from, to, maxEle, Size size) {
    List<PaintedCp> cpsToPaint = [];

    for (Cp cp in cps) {
      double cpDx = dx(size.width, cp.fromStart, from, to);
      double cpDy = dy(
          size.height,
          ElevationProvider.getElevationAtDistance(profile, cp.fromStart),
          maxEle);
      cpsToPaint.add(PaintedCp(cp: cp, dx: cpDx, dy: cpDy));
    }
    return cpsToPaint;
  }

  /// Returns the x value in viewport coordinates for the given [locationInKm].
  static double dx(double viewportWidth, double locationInKm, double from, to) {
    return viewportWidth * (locationInKm - from) / (to - from);
  }

  /// Returns the y value in viewport coordinates for the given [elevationInM],
  /// taking into account any padding for the x-axis.
  static double dy(double viewportHeight, double elevationInM, double maxEle) {
    double availableHeight =
        viewportHeight - PROFILE_BOTTOM_PADDING - PROFILE_TOP_PADDING;
    double startHeight = viewportHeight - PROFILE_BOTTOM_PADDING;

    return startHeight - availableHeight * elevationInM / maxEle;
  }

  /// Same as [dx] but a member function, to keep code tidy
  double _dx(double viewportWidth, double locationInKm) =>
      dx(viewportWidth, locationInKm, from, to);

  /// Same as [dy] but a member function, to keep code tidy
  double _dy(double viewportWidth, double locationInKm) =>
      dy(viewportWidth, locationInKm, maxEle);
}

/// Returns a sublist of rows from the elevation profile that lie within the given range.
///
/// The list includes a point outside the range on both sides.
/// This will result in smooth drawing across the screen edge
/// (rather than the profile abruptly stopping a few pixels before the edge
/// when a point does not lie exactly on the edge).
///
/// For optimisation reasons, if the range is above [THRESHOLD_DROP_POINTS],
/// some points will be dropped.
List<List> getPointsInRange(List<List> profile, double from, double to) {
  List<List> newProfile = [];

  List? prevRow;
  bool prevInRange = false;

  for (List row in profile) {
    if (inRange(row[colDist].toDouble(), from, to)) {
      // Inside profile
      if (!prevInRange && prevRow != null) {
        // If this is the start of the profile, add the previous point
        newProfile.add(prevRow);
      }
      newProfile.add(row);
      prevInRange = true;
    } else if (prevInRange) {
      // The profile just ended, add one more point
      newProfile.add(row);
      prevInRange = false;
    } else {
      // Outside profile: either before, or more than one point after
      prevRow = row;
      prevInRange = false;
    }
  }

  // If the range is large, drop every 2nd point to speed up painting.
  // One could consider dropping different numbers of points at different ranges/
  // zoom levels, but the added complexity is not worth the gain.
  double range = to - from;
  if (range > THRESHOLD_DROP_POINTS) {
    print("[ElePaint] Dropping points since $range is above threshold...");
    bool toggle = true;
    // Keep the first and the last points, since they are close to the screen edge.
    // The profile contains 100s of points even at the closest zoom level,
    // so skipping a few on the edges is safe.
    assert(newProfile.length > 4);
    // Filter from back to front to avoid nasty bugs!
    for (int i = newProfile.length - 4; i > 3; i--) {
      if (toggle) {
        newProfile.removeAt(i);
      }
      toggle = !toggle;
    }
  }

  return newProfile;
}

/// Returns true if the given [value] lies in the range from [from] to [to]
bool inRange(double value, double from, double to) {
  return value >= from && value <= to;
}
