import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/physics.dart';
import 'package:provider/provider.dart';
import 'package:spartathlon_app/models/Cp.dart';
import 'package:spartathlon_app/models/DistanceUnitChangeNotifier.dart';
import 'package:spartathlon_app/providers/CpProvider.dart';
import 'package:spartathlon_app/providers/ElevationProvider.dart';
import 'package:spartathlon_app/util/Themes.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/views/elevation/ElevationPainter.dart';
import 'package:spartathlon_app/views/elevation/PaintedCp.dart';
import 'package:spartathlon_app/widgets/CpDetailsBody.dart';

/// Distances to Athens/Sparta in km
/// Slightly rounded to the outside, to account for differences in datasets
const double DISTANCE_ATHENS = 0;
const double DISTANCE_SPARTA = 247;

/// How many km the user is allowed to overscroll
const double OVERSCROLL = 5;

/// Minimum range in km beyond that the user should not be able to zoom
const double MIN_ZOOM_RANGE = 10;
const double MAX_ZOOM_RANGE = DISTANCE_SPARTA + 2 * OVERSCROLL;

/// Displays an elevation profile of the race course.
///
/// Features (will) include:
/// - show distances and town names
/// - zoom in and moved along
/// - show checkpoints (at different granularity depending on the zoom level)
/// - show the live position of the favourite athletes during the race.
///
/// There are two animations:
/// - zooming upon double tap. Done by interpolating between the start and target
///   ranges given in KM
/// - flinging to a halt after a horizontal scroll. Done by listening to the
///   AnimationControllers values changing from 0 to 1 and mapping them to a
///   change in pixels to scroll by.
///   TODO This could be improved by using a Tween as well, though then we would
///        need to calculate the target distance ourselves.
class ElevationView extends StatefulWidget {
  ElevationView({Key? key}) : super(key: key);

  @override
  _ElevationViewState createState() => _ElevationViewState();
}

class _ElevationViewState extends State<ElevationView>
    with TickerProviderStateMixin {
  List<List<dynamic>> _profile = [];

  // Range of visible profile
  double _from = DISTANCE_ATHENS - OVERSCROLL;
  double _to = DISTANCE_SPARTA + OVERSCROLL;

  late AnimationController _zoomController;

  // Old range before a zoom action started
  double _zoomOldFrom = 0;
  double _zoomOldTo = 0;

  // Fling/drag animation fields
  late AnimationController _scrollController;
  double _lastControllerValue = 0;
  double _flingDirection = 0;

  // Handle to the CustomPaint widget, whose size is used to resolve taps and scrolls.
  // Since it is used in places where we can assume that the widget is visible
  // and has a context (e.g. in response to a user tapping the widget),
  // we boldly skip checking whether the context exists for these usages.
  GlobalKey _paintKey = GlobalKey();

  @override
  void initState() {
    _scrollController = AnimationController(vsync: this)
      // Update range when animation value changes
      ..addListener(() {
        setState(() {
          // Reset base value if controller starts anew
          if (_scrollController.value == 0) {
            _lastControllerValue = 0;
          }
          // The AnimationController indicates how many pixels we should scroll
          // in terms of the "percentage" of the graph's width.
          // Use the delta to calculate the percentage change between this and
          // the previous controller.value
          double delta = _scrollController.value - _lastControllerValue;
          double viewportWidth = _paintKey.currentContext!.size!.width;
          double pixelsToScroll = _flingDirection * delta * viewportWidth;

          _scrollViewportBy(pixelsToScroll);
          _lastControllerValue = _scrollController.value;
        });
      });

    _zoomController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<RouteCsv>(
      future: ElevationProvider.getElevationCsv(context),
      builder: (BuildContext context, AsyncSnapshot<RouteCsv> snapshot) {
        if (snapshot.hasData) {
          _profile = snapshot.data!;

          return GestureDetector(
            onDoubleTap: _onDoubleTap,
            onHorizontalDragStart: _onHorizontalDragStart,
            onHorizontalDragUpdate: _onHorizontalDragUpdate,
            onHorizontalDragEnd: _onHorizontalDragEnd,
            onScaleStart: _onScaleStart,
            onScaleUpdate: _onScaleUpdate,
            onTapDown: _onTapDown,
            child: Consumer<DistanceUnitChangeNotifier>(
              builder: (context, distanceUnitChangeNotifier, child) {
                return CustomPaint(
                  key: _paintKey,
                  painter: ElevationPainter(
                    profile: _profile,
                    unit: distanceUnitChangeNotifier.distanceUnit,
                    from: _from,
                    to: _to,
                    // TODO move this into the Themes
                    markerColor: markerColor(context),
                    markerColorLight: markerColorLight(context),
                    elevationLineColor: Theme.of(context).primaryColor,
                  ),
                  size: Size.infinite,
                );
              },
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _zoomController.dispose();
    super.dispose();
  }

  /// Upon double tap:
  ///
  /// - if the full range is visible: zoom to the level where minor CPs are visible
  /// - else zoom out to the full range
  void _onDoubleTap() {
    //print("Got double tap");
    double maxRange = DISTANCE_SPARTA + 2 * OVERSCROLL;
    double currentRange = _to - _from;
    // Subtract a little bit to avoid the double getting to big and CPs not showing
    double targetZoomRange = THRESHOLD_SHOW_MINOR_CPS.toDouble() - 2;

    // Update state but no need to notify framework
    _zoomOldFrom = _from;
    _zoomOldTo = _to;

    Tween tween;
    if (currentRange < maxRange) {
      // Animate zoom out
      tween = Tween<double>(begin: currentRange, end: maxRange);
    } else {
      // Animate zoom in
      tween = Tween<double>(begin: currentRange, end: targetZoomRange);
    }

    _zoomController.reset();
    Animation zoomAnimation = tween.animate(_zoomController);
    zoomAnimation.addListener(() {
      _zoomToRange(zoomAnimation.value);
    });
    _zoomController.forward();
  }

  void _onHorizontalDragStart(DragStartDetails details) {
    // Stop animation and give control over movement back to the user
    if (_scrollController.isAnimating) {
      _scrollController.stop();
      _scrollController.reset();
    }
  }

  /// Handles scrolling the (zoomed) profile horizontally
  void _onHorizontalDragUpdate(DragUpdateDetails details) {
    double delta = details.primaryDelta!;
    _scrollViewportBy(delta);
  }

  /// Handles animating the scrolling coming slowly to a halt
  void _onHorizontalDragEnd(DragEndDetails details) {
    // Velocity relative to the unit interval used by the scroll controller
    // Inspired by https://flutter.dev/docs/cookbook/animation/physics-simulation
    Size size = _paintKey.currentContext!.size!;
    final unitsPerSecondX = details.velocity.pixelsPerSecond.dx / size.width;
    final unitsPerSecondY = details.velocity.pixelsPerSecond.dy / size.height;
    final unitsPerSecond = Offset(unitsPerSecondX, unitsPerSecondY);
    final unitVelocity = unitsPerSecond.distance;

    // Set horizontal fling direction
    _flingDirection = unitsPerSecondX.sign;

    // Friction coefficient was chosen by what looks nice
    final frictionSimulation = FrictionSimulation(0.005, 0, unitVelocity);

    // Start animation that slowly brings the fling to a halt
    _scrollController.animateWith(frictionSimulation);
  }

  /// [pixelDelta] - the number of pixels the viewport should move.
  ///                The sign indicates the direction.
  void _scrollViewportBy(double pixelDelta) {
    double viewportWidth = _paintKey.currentContext!.size!.width;
    // Delta distance in km
    // Relative to the visible range in order to avoid overly fast scrolling in high zoom levels
    double distanceDelta = (_to - _from) * pixelDelta / viewportWidth;

    // The distanceDelta is positive if the pointer from left to right.
    // So subtract in order to move the visible profile to the left
    double newFrom = _from - distanceDelta;
    double newTo = _to - distanceDelta;
    // print("from=$_from, newFrom=$newFrom, to=$_to, newTo=$newTo");

    // If we would over-scroll, just scroll to the maximum possible value
    if (newFrom < DISTANCE_ATHENS - OVERSCROLL) {
      newFrom = DISTANCE_ATHENS - OVERSCROLL;
      newTo = _to - (_from - newFrom).abs();
    } else if (newTo > DISTANCE_SPARTA + OVERSCROLL) {
      newTo = DISTANCE_SPARTA + OVERSCROLL;
      newFrom = _from + (newTo - _to).abs();
    }

    assert(newFrom >= DISTANCE_ATHENS - OVERSCROLL);
    assert(newTo <= DISTANCE_SPARTA + OVERSCROLL);
    setState(() {
      _from = newFrom;
      _to = newTo;
    });
  }

  // Saves the distance range at the start of the zoom action.
  // These are used to calculate the relative new zoomed distances.
  void _onScaleStart(ScaleStartDetails details) {
    // Update state but no need to notify framework
    _zoomOldFrom = _from;
    _zoomOldTo = _to;
  }

  /// Handles zooming into the profile
  void _onScaleUpdate(ScaleUpdateDetails details) {
    //print("[Zoom] $details");
    if (details.scale == 0) {
      return;
    }
    double oldRange = _zoomOldTo - _zoomOldFrom;
    double newRange = oldRange / details.scale;
    _zoomToRange(newRange);
  }

  /// Updates the state to a zoom from the cached range to the [newRange]
  void _zoomToRange(double newRange) {
    double oldRange = _zoomOldTo - _zoomOldFrom;
    assert(oldRange > 0);
    assert(newRange > 0);
    //print("[Zoom] oldRange: $oldRange newRange: $newRange");

    // Limit zoom level
    // TODO allow some overzoom but then snap back to the minimum range
    if (newRange < MIN_ZOOM_RANGE) {
      newRange = MIN_ZOOM_RANGE;
    } else if (newRange > MAX_ZOOM_RANGE) {
      newRange = MAX_ZOOM_RANGE;
    }
    //print("[Zoom] final newRange: $newRange");

    // The delta is positive if zooming out, i.e. when a greater range of the
    // profile should be shown. Conversely, it is negative if zooming in.
    double delta = newRange - oldRange;
    //print("[Zoom] delta: $delta");

    double newFrom, newTo;
    if (delta > 0) {
      // Zoom out
      //
      // Give delta/2 to the both edges, or up to nothing if it is already on the boundary.
      // Then give the remaining delta to the other edge.
      //
      // If we were to simply give delta/2 to equally both sides, then if the focal point
      // is not centered, it is very hard to impossible to fully zoom out.
      // E.g. if we are zoomed into the range [200, 250], then we would waste half
      // our delta on every zoom. Then zoom would appear slow to the user.
      double remainingToTheLeft = _zoomOldFrom - (DISTANCE_ATHENS - OVERSCROLL);
      double remainingToTheRight = (DISTANCE_SPARTA + OVERSCROLL) - _zoomOldTo;

      double deltaLeft = 0;
      double deltaRight = 0;
      if (remainingToTheLeft > 0) {
        deltaLeft = min(delta / 2, remainingToTheLeft);
      }
      if (remainingToTheRight > 0) {
        deltaRight = min(delta / 2, remainingToTheRight);
      }

      // How much of their delta both edges did not use (since they were already on their boundary)
      double unusedLeft = delta / 2 - deltaLeft;
      double unusedRight = delta / 2 - deltaRight;

      // Update both edges with what the delta they normally get, plus any left-over
      // delta donated by the other edge.
      // With this point we might over-zoom, but this is fixed by clamping below.
      newFrom = _zoomOldFrom - deltaLeft - unusedRight;
      newTo = _zoomOldTo + deltaRight + unusedLeft;
    } else {
      // Zoom in
      newFrom = _zoomOldFrom - delta / 2;
      newTo = _zoomOldTo + delta / 2;
    }
    //print("[Zoom] from($oldFrom -> $newFrom) to($oldFrom -> $newTo)");

    // Don't zoom outside the profile, clamp the value to the edges
    newFrom = max(DISTANCE_ATHENS - OVERSCROLL, newFrom);
    newTo = min(DISTANCE_SPARTA + OVERSCROLL, newTo);

    assert(newFrom >= DISTANCE_ATHENS - OVERSCROLL);
    assert(newTo <= DISTANCE_SPARTA + OVERSCROLL);
    // Check that we didn't loose any delta and are zooming to little
    // +/- 2 because we are working with floating point numbers
    assert(newTo - newFrom >= newRange - 2 && newTo - newFrom <= newRange + 2);
    setState(() {
      _from = newFrom;
      _to = newTo;
    });
  }

  /// Handles tapping onto the profile (hopefully onto a CP)
  void _onTapDown(TapDownDetails details) {
    //print("Got onTapDown: ${details.localPosition}");

    // Find all CPs currently painted
    double maxEle = ElevationProvider.getMinMaxElevation(_profile).item2;
    List<Cp> cps;
    // Why is `if` not an expression in Dart? :(
    if (_to - _from > THRESHOLD_SHOW_MINOR_CPS) {
      cps = CpProvider.getMajorCps();
    } else {
      cps = CpProvider.getCps();
    }

    List<PaintedCp> paintedCps = ElevationPainter.getPaintedCps(
        _profile, cps, _from, _to, maxEle, _paintKey.currentContext!.size!);

    // Find the tapped one
    Cp? tappedCp;
    for (PaintedCp pCp in paintedCps) {
      if (pCp.isHitBy(details.localPosition.dx, details.localPosition.dy)) {
        tappedCp = pCp.cp;
        break;
      }
    }
    //print("Tapped $tappedCp");

    if (tappedCp != null) {
      _showBottomSheet(tappedCp);
    }
  }

  /// Shows a bottom sheet containing info on [cp]
  void _showBottomSheet(Cp cp) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        // Without Wrap the bottom sheet would default to half the screen height
        return Wrap(
          children: [
            Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.only(left: 16, top: 20, right: 16, bottom: 20),
                  child: Row(
                    children: [
                      Text(
                        "CP ${cp.id}",
                        style: TextStyle(fontSize: 16),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: Text(
                            cp.name,
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ),
                      cp.getIconsWidget(context),
                    ],
                  ),
                ),
                CpDetailsBody(
                  cp: cp,
                  distanceUnit: Provider.of<DistanceUnitChangeNotifier>(
                    context,
                    listen: false, // since the bottom sheet is "one-shot"
                  ).distanceUnit,
                ),
              ],
            )
          ],
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(kCardCornerRadius),
        ),
      ),
    );
  }
}
