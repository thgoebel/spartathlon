import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/AppStateContainer.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/views/AthletesView.dart';
import 'package:spartathlon_app/views/course/CourseView.dart';
import 'package:spartathlon_app/views/dashboard/DashboardView.dart';
import 'package:spartathlon_app/views/elevation/ElevationView.dart';
import 'package:spartathlon_app/views/settings/SettingsView.dart';
import 'package:spartathlon_app/widgets/ErrorBar.dart';

/// Main view for Spartathlon app
///
/// 'Main' contains the broad frame the app is set it: the top and bottom app bar.
/// It enables switching between the four major screens.

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  int _index = AppPage.DASHBOARD.index;

  List<Widget> _pages = [
    DashboardView(),
    CourseView(),
    ElevationView(),
    AthletesView(),
  ];

  @override
  Widget build(BuildContext context) {
    AppStateContainer.of(context).checkConnectionState();

    return Scaffold(
      appBar: AppBar(
        title: Text(SpartaLocalizations.of(context).appName),
        actions: [
          IconButton(
              icon: Icon(Icons.settings_outlined),
              tooltip: SpartaLocalizations.of(context).tooltipSettings,
              onPressed: () {
                Navigator.push(context,
                    CupertinoPageRoute(builder: (context) => SettingsView()));
              }),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _getNavBarIcons(context),
        currentIndex: _index,
        onTap: _onTabTapped,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white,
      ),
      body: _getBody(context),
    );
  }

  _onTabTapped(int index) {
    setState(() {
      _index = index;
    });
  }

  List<BottomNavigationBarItem> _getNavBarIcons(BuildContext context) {
    return [
      BottomNavigationBarItem(
        icon: Icon(Icons.dashboard_outlined),
        label: SpartaLocalizations.of(context).sectionDashboard,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.near_me_outlined),
        label: SpartaLocalizations.of(context).sectionCourse,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.show_chart_outlined),
        label: SpartaLocalizations.of(context).sectionElevation,
        backgroundColor: Theme.of(context).primaryColor,
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.directions_run_outlined),
        label: SpartaLocalizations.of(context).sectionAthletes,
        backgroundColor: Theme.of(context).primaryColor,
      ),
    ];
  }

  Widget _getBody(BuildContext context) {
    Widget mainBody = IndexedStack(
      index: _index,
      children: _pages,
    );

    if (AppStateContainer.of(context).data.hasConnection) {
      return mainBody;
    } else {
      return Stack(
        children: [
          // Central page
          Container(
            padding: EdgeInsets.only(top: kNoNetworkBarHeight),
            child: mainBody,
          ),
          ErrorBar(
            errorText:
                SpartaLocalizations.of(context).titleInfoBarNoNetworkConnection,
            retryText: SpartaLocalizations.of(context)
                .labelButtonInfoBarNoNetworkConnection,
            onRetryPressed: () => AppStateContainer.of(context).loadData(false),
          ),
        ],
      );
    }
  }
}

/// Possible pages in the app that can be navigated to through the BottomNavigationBar
///
/// The order is important since it defines the order in which they appear
/// in the BottomNavigationBar.
enum AppPage {
  DASHBOARD,
  COURSE,
  ELEVATION,
  ATHLETES,
}
