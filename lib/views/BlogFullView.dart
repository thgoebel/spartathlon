import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:spartathlon_app/models/BlogPost.dart';
import 'package:spartathlon_app/util/Utils.dart';

/// Blog Details View
///
/// Displays a blog entry (race report) with the header image at the top,
/// followed by the report itself.

class BlogFullView extends StatefulWidget {
  BlogFullView({Key? key, required this.item}) : super(key: key);
  final BlogPost item;

  @override
  _BlogFullViewState createState() => _BlogFullViewState();
}

class _BlogFullViewState extends State<BlogFullView> {
  late ScrollController _scrollController;
  double kExpandedHeight = 1;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController()..addListener(() => setState(() {}));
  }

  bool isCollapsed() {
    return _scrollController.hasClients &&
        _scrollController.offset > kExpandedHeight - 1.7 * kToolbarHeight;
  }

  @override
  Widget build(BuildContext context) {
    Widget flexibleSpace = Container();

    final imageUrl = widget.item.imageUrl;
    if (imageUrl != null) {
      kExpandedHeight = MediaQuery.of(context).size.height / 2;
      flexibleSpace = FlexibleSpaceBar(
        background: CachedNetworkImage(
          imageUrl: imageUrl,
          fit: BoxFit.fitWidth,
          alignment: Alignment(0, 0.5),
        ),
      );
    }

    return Scaffold(
      body: CustomScrollView(
        controller: _scrollController,
        slivers: [
          SliverAppBar(
            backgroundColor: Theme.of(context).primaryColor,
            expandedHeight: kExpandedHeight,
            floating: false,
            pinned: true,
            actions: [
              PopupMenuButton(
                itemBuilder: (BuildContext context) {
                  return [
                    PopupMenuItem(
                      child: Text(SpartaLocalizations.of(context)
                          .menuActionOpenInBrowser),
                      value: widget.item.url,
                    )
                  ];
                },
                onSelected: (String url) => launchUrl(url),
              )
            ],
            flexibleSpace: flexibleSpace,
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Text(
                    widget.item.title,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                ),
                Material(
                  child: Container(
                    color: Theme.of(context).scaffoldBackgroundColor,
                    padding: EdgeInsets.only(left: 12, right: 12, bottom: 12),
                    child: Html(
                      data: widget.item.fullText,
                      onLinkTap: (url, _, __, ___) =>
                          launchUrl(spartafyUrl(url)),
                      style: {
                        "html": Style(
                          backgroundColor:
                              Theme.of(context).scaffoldBackgroundColor,
                        ),
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
