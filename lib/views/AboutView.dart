import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/providers/LicenseData.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/widgets/SpartaCard.dart';
import 'package:spartathlon_app/widgets/SpartaDivider.dart';

/// About View
///
/// This view displays info about the race itself, the app and credits

String versionName = '0.8.2';

class AboutView extends StatefulWidget {
  AboutView({Key? key}) : super(key: key);

  @override
  _AboutViewState createState() => _AboutViewState();
}

class _AboutViewState extends State<AboutView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(SpartaLocalizations.of(context).appBarTitleAbout),
      ),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: _getBody(),
    );
  }

  Widget _getBody() {
    return ListView(
      children: [
        SpartaCard(
          margin: EdgeInsets.all(12.0),
          child: Column(
            children: [
              ListTile(
                title: Text(
                  SpartaLocalizations.of(context).titleAboutTheRace,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500),
                ),
              ),
              SpartaDivider(width: 1.5),
              Padding(
                padding: EdgeInsets.all(12.0),
                child: Text(
                  SpartaLocalizations.of(context).textRaceDescription,
                ),
              ),
              SpartaDivider(),
              ListTile(
                leading: Icon(Icons.language,
                    color: Theme.of(context).iconTheme.color),
                trailing: Icon(Icons.open_in_new,
                    color: Theme.of(context).iconTheme.color),
                title: Text(
                  SpartaLocalizations.of(context).labelAboutWebsite,
                ),
                onTap: () => launchUrl('https://spartathlon.gr/en'),
              )
            ],
          ),
        ),
        SpartaCard(
          margin: EdgeInsets.only(bottom: 12.0, left: 12.0, right: 12.0),
          child: Column(
            children: [
              ListTile(
                title: Text(
                  SpartaLocalizations.of(context).titleAboutThisApp,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500),
                ),
              ),
              SpartaDivider(width: 1.5),
              ListTile(
                leading: Icon(Icons.info_outline,
                    color: Theme.of(context).iconTheme.color),
                title: Text(
                  SpartaLocalizations.of(context).labelAboutVersion,
                ),
                trailing: Text(versionName),
              ),
              SpartaDivider(),
              ListTile(
                leading: Icon(Icons.person_outline,
                    color: Theme.of(context).iconTheme.color),
                title: Text(SpartaLocalizations.of(context).labelAboutAuthor),
                trailing: Text('Thore Goebel'),
              ),
              SpartaDivider(),
              ListTile(
                leading: Icon(Icons.copyright,
                    color: Theme.of(context).iconTheme.color),
                title: Text(
                  SpartaLocalizations.of(context).labelAboutLicenses,
                ),
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (context) {
                      return SimpleDialog(
                        title: Text(
                          SpartaLocalizations.of(context).labelAboutLicenses,
                        ),
                        children: [
                          Container(
                            color: Theme.of(context).dialogBackgroundColor,
                            constraints: BoxConstraints.tightFor(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height - 300,
                            ),
                            child: ListView(children: _getLicenseTiles()),
                          ),
                        ],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0)),
                      );
                    },
                  );
                },
              ),
              SpartaDivider(),
              ListTile(
                leading: Icon(
                  Icons.security,
                  color: Theme.of(context).primaryColor,
                ),
                trailing: Icon(
                  Icons.open_in_new,
                  color: Theme.of(context).primaryColor,
                ),
                title: Text(
                  SpartaLocalizations.of(context).labelAboutPrivacyPolicy,
                ),
                onTap: () => launchUrl(
                    'https://gitlab.com/thgoebel/spartathlon/blob/main/PRIVACY.md'),
              ),
              SpartaDivider(),
              ListTile(
                leading: Icon(Icons.restore,
                    color: Theme.of(context).iconTheme.color),
                trailing: Icon(Icons.open_in_new,
                    color: Theme.of(context).iconTheme.color),
                title: Text(
                  SpartaLocalizations.of(context).labelAboutChangelog,
                ),
                onTap: () => launchUrl(
                  'https://gitlab.com/thgoebel/spartathlon/blob/main/CHANGELOG.md',
                ),
              ),
              SpartaDivider(),
              ListTile(
                leading:
                    Icon(Icons.code, color: Theme.of(context).iconTheme.color),
                trailing: Icon(Icons.open_in_new,
                    color: Theme.of(context).iconTheme.color),
                title: Text(
                  SpartaLocalizations.of(context).labelAboutSourceCode,
                ),
                onTap: () =>
                    launchUrl('https://gitlab.com/thgoebel/spartathlon'),
              ),
            ],
          ),
        ),
      ],
    );
  }

  /// Return a list of tiles to be used in the popup dialog displaying all licenses
  List<Widget> _getLicenseTiles() {
    // [licenses] (defined in LicenseData.dart) hold the metadata for each license
    List<Widget> licenseTiles = licenses
        .map((Map<String, Object> e) => Container(
              height: kLicenseTileHeight,
              child: Stack(
                children: [
                  Container(
                      padding: EdgeInsets.only(left: 6.0),
                      child: ListTile(
                        title: Text(e['name'].toString() +
                            ' by ' +
                            e['author'].toString()),
                        subtitle: Text(e['license'].toString()),
                      )),
                  Material(
                    type: MaterialType.transparency,
                    child: InkWell(
                      onTap: () => launchUrl(e['url'].toString()),
                    ),
                  ),
                ],
              ),
            ))
        .toList();
    return licenseTiles;
  }
}
