import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:intl/intl.dart';
import 'package:spartathlon_app/AppStateContainer.dart';
import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/providers/AthleteProvider.dart';
import 'package:spartathlon_app/util/RaceTiming.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:spartathlon_app/views/general/ErrorView.dart';
import 'package:spartathlon_app/widgets/AthleteDetailsListTile.dart';
import 'package:spartathlon_app/widgets/CustomFinishTile.dart';
import 'package:spartathlon_app/widgets/SpartaCard.dart';
import 'package:spartathlon_app/widgets/SpartaDivider.dart';

/// Athletes View
///
/// Displays Material cards giving details about a specific athlete such as
/// nationality, date of birth, their biography and list of finishes

final double marginValue = 12.0;

class AthleteDetailsView extends StatefulWidget {
  AthleteDetailsView({Key? key, required this.athlete}) : super(key: key);

  final Athlete athlete;

  @override
  _AthleteDetailsViewState createState() => _AthleteDetailsViewState();
}

class _AthleteDetailsViewState extends State<AthleteDetailsView> {
  // State-local athlete that can be updated when all of its details are fetched
  // needed since the `widget.athlete` should be final (as all Widget-fields)
  // TODO With a better backend, the full athlete details are returned on the first request
  // Note that in places where the details are already there (and a second request
  // is never needed), we use `widget.athlete` (for quicker migration later).
  late Athlete _athlete;
  late ScrollController _scrollController;
  double kExpandedHeight = 1;

  @override
  void initState() {
    super.initState();
    _athlete = widget.athlete;
    _scrollController = ScrollController()..addListener(() => setState(() {}));
  }

  bool isCollapsed() {
    return _scrollController.hasClients &&
        _scrollController.offset > kExpandedHeight - 1.4 * kToolbarHeight;
  }

  @override
  Widget build(BuildContext context) {
    kExpandedHeight = MediaQuery.of(context).size.height / 2;

    return Scaffold(
      body: CustomScrollView(
        controller: _scrollController,
        slivers: [
          SliverAppBar(
            backgroundColor: Theme.of(context).primaryColor,
            expandedHeight: kExpandedHeight,
            floating: false,
            pinned: true,
            actions: [
              FutureBuilder<bool>(
                future: widget.athlete.isFavorite(),
                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                  if (snapshot.hasData) {
                    return IconButton(
                        icon: (snapshot.data!)
                            ? Icon(
                                Icons.star,
                                color: Colors.white,
                              )
                            : Icon(
                                Icons.star_border,
                                color: Colors.white,
                              ),
                        onPressed: () {
                          setState(() {
                            AppStateContainer.of(context)
                                .setToggleFavoriteAthlete(widget.athlete.id);
                          });
                        });
                  } else {
                    return Container();
                  }
                },
              ),
            ],
            flexibleSpace: FlexibleSpaceBar(
                title: Text(widget.athlete.name),
                titlePadding: getPlatformDependentTitlePadding(isCollapsed()),
                background: Stack(
                  fit: StackFit.expand,
                  children: [
                    CachedNetworkImage(
                      imageUrl: widget.athlete.imageUrl,
                      fadeInDuration: Duration(milliseconds: 0),
                      fit: BoxFit.cover,
                      alignment: Alignment(0, 0.5),
                    ),
                    GestureDetector(onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return SimpleDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16.0)),
                            contentPadding: EdgeInsets.all(8.0),
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(10.0),
                                child: CachedNetworkImage(
                                  imageUrl: widget.athlete.imageUrl,
                                  fit: BoxFit.contain,
                                ),
                              )
                            ],
                          );
                        },
                      );
                    })
                  ],
                )),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                (_athlete.isFullyLoaded)
                    ? _getFullBody(_athlete)
                    : FutureBuilder(
                        future: AthleteProvider.getFullAthleteDetails(
                            widget.athlete),
                        builder: (BuildContext context,
                            AsyncSnapshot<Athlete> snapshot) {
                          if (snapshot.hasData) {
                            _athlete = snapshot.data!;
                            return _getFullBody(_athlete);
                          } else if (snapshot.hasError) {
                            return Container(
                              height: kExpandedHeight,
                              child: ErrorView(
                                // Cause widget to rebuild and thus attempt to fetch details again
                                onRetry: (() => setState(() {})),
                                text: SpartaLocalizations.of(context)
                                    .errorNotLoaded,
                              ),
                            );
                          } else {
                            return Container(
                              height: kExpandedHeight,
                              color: Theme.of(context).scaffoldBackgroundColor,
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                        }),
              ],
            ),
          ),
        ],
      ),
    );
  }

  // TODO Move the cards into separate StatelessWidgets (better testabilty)

  Widget _getFullBody(Athlete a) {
    return Material(
      child: Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        constraints:
            BoxConstraints(minHeight: MediaQuery.of(context).size.height - 80),
        child: Column(
          children: [
            _getFactsCard(a),
            _getBiographyCard(a),
            _getFinishesCard(a),
          ],
        ),
      ),
    );
  }

  /// Return a card with the athletes nationality and birthday
  Widget _getFactsCard(Athlete a) {
    return SpartaCard(
      margin: EdgeInsets.all(marginValue),
      child: Column(
        children: _getFactListTiles(a),
      ),
    );
  }

  /// Returns a list of AthleteDetailsListTiles.
  /// Some are always added (like the athletes birthday and country), others are
  /// only added under curtain conditions (like the live timing).
  List<Widget> _getFactListTiles(Athlete a) {
    List<Widget> factListTiles = [
      AthleteDetailsListTile(
        leading: Icon(
          Icons.public,
          color: Theme.of(context).primaryColor,
        ),
        supertitle: Text(
          SpartaLocalizations.of(context).titleCountry,
          style: supertitleStyle,
        ),
        title: Text(a.nation, style: titleStyle),
      ),
      SpartaDivider(),
      AthleteDetailsListTile(
        leading: Icon(
          Icons.cake_outlined,
          color: Theme.of(context).primaryColor,
        ),
        supertitle: Text(
          SpartaLocalizations.of(context).titleDateOfBirth,
          style: supertitleStyle,
        ),
        title: Text(
          DateFormat('yMd').format(DateTime.parse(a.dateOfBirth)),
          style: titleStyle,
        ),
      ),
    ];

    if (isRaceOn()) {
      factListTiles.add(SpartaDivider());
      factListTiles.add(AthleteDetailsListTile(
        leading: Icon(
          Icons.insert_chart_outlined,
          color: Theme.of(context).primaryColor,
        ),
        supertitle: Text(
          SpartaLocalizations.of(context).titleLiveTiming,
          style: supertitleStyle,
        ),
        title: Text(
          a.lastSeenTime + ' at CP ' + a.lastSeenCp.toString(),
          style: titleStyle,
        ),
      ));
    }
    return factListTiles;
  }

  /// Returns a Material card with the athletes biography (aka Mini CV)
  Widget _getBiographyCard(Athlete a) {
    if (a.biography.isNotEmpty) {
      return SpartaCard(
        margin: EdgeInsets.only(
            right: marginValue, left: marginValue, bottom: marginValue),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.all(16.0),
              child: Text(
                SpartaLocalizations.of(context).titleBiography,
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 16.0),
              ),
            ),
            SpartaDivider(),
            Container(
              padding: EdgeInsets.all(16.0),
              child: Text(a.biography),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  /// Returns a Material card with a list of finishes for athlete a
  Widget _getFinishesCard(Athlete a) {
    return SpartaCard(
      margin: EdgeInsets.only(
          right: marginValue, left: marginValue, bottom: marginValue),
      child: Column(
        children: _getFinishTiles(a),
      ),
    );
  }

  /// Return a Material Card with a list of finishes
  List<Widget> _getFinishTiles(Athlete a) {
    List<Widget> list = [
      Container(
        padding: (a.finishes.isNotEmpty)
            ? EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0, bottom: 0.0)
            : EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0, bottom: 10.0),
        // For some reason these need to be different by 1 otherwise Flutter
        // will draw a black line of height 1 between the bio and the finishes card
        child: Row(
          children: [
            Expanded(
              child: Text(
                SpartaLocalizations.of(context).titleFinishes,
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontSize: 16.0,
                ),
              ),
            ),
            Text(
              a.finishes.length.toString(),
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 16.0,
              ),
            ),
          ],
        ),
      ),
    ];
    if (a.finishes.isNotEmpty) {
      List<Widget> finishListHeaderItemList = [
        CustomFinishTile(Finish(
          year: SpartaLocalizations.of(context).headerFinishesListYear,
          time: SpartaLocalizations.of(context).headerFinishesListTime,
          bib: SpartaLocalizations.of(context).headerFinishesListBibNr,
        )),
        Padding(padding: EdgeInsets.only(bottom: 6.0)),
        SpartaDivider(),
      ];
      List<CustomFinishTile> finishTiles = List<Finish>.from(a.finishes)
          .map((Finish f) => CustomFinishTile(f))
          .toList();

      list.addAll(finishListHeaderItemList);
      list.addAll(finishTiles);
    }
    list.add(Container(height: 8.0)); // Bottom padding
    return list;
  }
}
