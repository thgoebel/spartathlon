import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:spartathlon_app/models/Event.dart';
import 'package:spartathlon_app/providers/EventProvider.dart';
import 'package:spartathlon_app/widgets/EventCard.dart';
import 'package:spartathlon_app/widgets/IconListDivider.dart';

const int DEFAULT_NUM_EVENTS_TO_SHOW = 2;

class EventList extends StatefulWidget {
  final List<Event> events;

  EventList({Key? key, required this.events}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  List<List<Event>> eventsToShow = [];
  bool showAll = false;
  int eventDays = 0;

  @override
  Widget build(BuildContext context) {
    if (widget.events.isEmpty ||
        !EventProvider.hasCurrentOrFutureEvents(
            widget.events, DateTime.now())) {
      return SliverToBoxAdapter(child: Container());
    }

    eventsToShow = _getEventsPerDayToShow(widget.events);

    return SliverStickyHeader.builder(
      builder: (_, state) => IconListDivider(state, Icon(Icons.calendar_today)),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate((_, index) {
          if (index == 0) {
            return Container(
              padding: EdgeInsets.only(
                  left: 16.0, top: 4.0, right: 16.0, bottom: 4.0),
              child: Text(
                SpartaLocalizations.of(context).textNoteAllTimesInLocalTime,
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            );
          } else if (index == eventsToShow.length + 1) {
            if (eventDays > DEFAULT_NUM_EVENTS_TO_SHOW) {
              String buttonText = (showAll)
                  ? SpartaLocalizations.of(context).labelButtonLess
                  : SpartaLocalizations.of(context).labelButtonMore;
              return Container(
                alignment: Alignment.center,
                child: TextButton(
                  onPressed: () => setState(() => showAll = !showAll),
                  child: Text(buttonText.toUpperCase()),
                ),
              );
            } else {
              return Container();
            }
          } else {
            return EventCard(events: eventsToShow[index - 1]);
          }
        }, childCount: eventsToShow.length + 2),
      ),
    );
  }

  List<List<Event>> _getEventsPerDayToShow(List<Event> events) {
    List<List<Event>> eventsPerDay =
        EventProvider.getEventsPerDay(events, DateTime.now());
    // TODO It's confusing to initialise this here ==> refactor
    eventDays = eventsPerDay.length;

    if (!showAll) {
      int end = min(DEFAULT_NUM_EVENTS_TO_SHOW, eventsPerDay.length);
      return eventsPerDay.sublist(0, end);
    } else {
      return eventsPerDay;
    }
  }
}
