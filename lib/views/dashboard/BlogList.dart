import 'package:flutter/material.dart';

import 'package:flutter_sticky_header/flutter_sticky_header.dart';

import 'package:spartathlon_app/models/BlogPost.dart';
import 'package:spartathlon_app/widgets/BlogEntryCard.dart';
import 'package:spartathlon_app/widgets/IconListDivider.dart';

class BlogList extends StatelessWidget {
  BlogList({Key? key, required this.blogEntries}) : super(key: key);

  final List<BlogPost> blogEntries;

  @override
  Widget build(BuildContext context) {
    if (blogEntries.isEmpty) return SliverToBoxAdapter(child: Container());

    return SliverStickyHeader.builder(
      builder: (_, state) => IconListDivider(state, Icon(Icons.message_outlined)),
      sliver: SliverList(
        delegate: SliverChildBuilderDelegate(
          (_, index) => BlogEntryCard(blogEntries[index]),
          childCount: blogEntries.length,
        ),
      ),
    );
  }
}
