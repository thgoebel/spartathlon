import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/AppStateContainer.dart';
import 'package:spartathlon_app/views/dashboard/BlogList.dart';
import 'package:spartathlon_app/views/dashboard/EventList.dart';
import 'package:spartathlon_app/views/dashboard/FavoriteAthletesSlider.dart';
import 'package:spartathlon_app/views/general/ErrorView.dart';
import 'package:spartathlon_app/views/general/LoadingView.dart';

/// Dashboard View
///
/// The dashboard is the screen the user first sees when opening the app.
/// It should display:
///
/// - any information important to the user
/// - general information not in any other subsection
///
/// This for example includes favorite athletes, blog entries, race countdown
/// and timer, race calendar, etc
class DashboardView extends StatefulWidget {
  DashboardView({Key? key}) : super(key: key);

  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  @override
  Widget build(BuildContext context) {
    AppStateContainerState container = AppStateContainer.of(context);

    print('Building Dashboard');

    if (container.data.isLoading) {
      return LoadingView();
    }

    if (container.data.hasError) {
      return ErrorView(
        onRetry: container.reloadAfterError,
        text: SpartaLocalizations.of(context).errorNotLoaded,
      );
    }

    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: RefreshIndicator(
        onRefresh: () => container.loadData(true),
        child: CustomScrollView(
          slivers: [
            // Workaround to prevent top IconListDivider to be hovered immediately
            SliverToBoxAdapter(child: Container(height: 0.1)),
            FavoriteAthletesSlider(
              favoriteAthletes: container.data.favoriteAthletes,
            ),
            EventList(events: container.data.events),
            BlogList(blogEntries: container.data.blogPosts),
          ],
        ),
      ),
    );
  }
}
