import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/util/RaceTiming.dart';
import 'package:spartathlon_app/widgets/FavAthleteCard.dart';
import 'package:spartathlon_app/widgets/IconListDivider.dart';

class FavoriteAthletesSlider extends StatelessWidget {
  final List<Athlete> favoriteAthletes;

  const FavoriteAthletesSlider({Key? key, required this.favoriteAthletes})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double cardSize = (isRaceOn()) ? 212 : 125; // Size of square card

    if (favoriteAthletes.isEmpty) {
      return SliverToBoxAdapter(child: Container());
    }

    return SliverStickyHeader.builder(
      builder: (_, state) => IconListDivider(state, Icon(Icons.directions_run)),
      sliver: SliverToBoxAdapter(
        child: Container(
          height: cardSize,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            padding: EdgeInsets.all(5.0),
            itemCount: favoriteAthletes.length,
            itemBuilder: (_, i) =>
                FavAthleteCard(favoriteAthletes[i], cardSize),
          ),
        ),
      ),
    );
  }
}
