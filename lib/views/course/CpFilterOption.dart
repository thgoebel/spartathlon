import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum CpFilterOption { All, Supporters, Live }

const allCpFilterOptions = [
  CpFilterOption.All,
  CpFilterOption.Supporters,
  CpFilterOption.Live
];

extension CpFilterOptionToString on CpFilterOption {
  String toHumanString(BuildContext context) {
    switch (this) {
      case CpFilterOption.Supporters:
        return SpartaLocalizations.of(context).caseCpFilterSupporters;
      case CpFilterOption.Live:
        return SpartaLocalizations.of(context).caseCpFilterLiveTiming;
      default:
        return SpartaLocalizations.of(context).caseCpFilterAll;
    }
  }
}

const String SETTING_CP_FILTER = 'cpFilter';

void setCpFilterOptionToPrefs(CpFilterOption option) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt(SETTING_CP_FILTER, option.index);
  // Ignore failure to save, e.g. on web
}

Future<CpFilterOption> getCpFilterOptionFromPref() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int optionIndex =
      prefs.getInt(SETTING_CP_FILTER) ?? CpFilterOption.All.index;
  return CpFilterOption.values[optionIndex];
}
