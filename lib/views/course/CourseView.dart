import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:spartathlon_app/models/Cp.dart';
import 'package:spartathlon_app/models/DistanceUnitChangeNotifier.dart';
import 'package:spartathlon_app/providers/CpProvider.dart';
import 'package:spartathlon_app/views/course/CourseListHeaderBar.dart';
import 'package:spartathlon_app/views/course/CpFilterOption.dart';
import 'package:spartathlon_app/widgets/CpDetailsBody.dart';

/// Course View
///
/// Lists all checkpoints (aka CPs in Spartathlon slang)
class CourseView extends StatefulWidget {
  CourseView({Key? key}) : super(key: key);

  @override
  _CourseViewState createState() => _CourseViewState();
}

class _CourseViewState extends State<CourseView> {
  List<Cp> cps = [];
  List<Cp> cpsToDisplay = [];

  @override
  void initState() {
    cps = CpProvider.getCps();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getCpFilterOptionFromPref(),
      builder: (BuildContext context, AsyncSnapshot<CpFilterOption> snapshot) {
        if (snapshot.hasData) {
          CpFilterOption option = snapshot.data!;
          cpsToDisplay = _filterCpList(cps, option);

          return Column(
            children: [
              // CP list header bar
              CourseListHeaderBar(
                selectedOption: option,
                onCpFilterOptionChanged: (CpFilterOption? selection) {
                  // Store last used filter in prefs so that we can auto-restore it
                  if (selection == null) return;
                  setCpFilterOptionToPrefs(selection);
                  setState(() {
                    cpsToDisplay = _filterCpList(cps, selection);
                  });
                },
              ),
              // CP list
              Expanded(
                child: Consumer<DistanceUnitChangeNotifier>(
                  builder: (context, distanceUnitChangeNotifier, child) {
                    return ListView.builder(
                      itemCount: cpsToDisplay.length,
                      itemBuilder: ((BuildContext context, int position) {
                        return ExpansionTile(
                          key: ValueKey(cpsToDisplay[position].id),
                          leading: Padding(
                              padding: EdgeInsets.only(top: 4.0),
                              child: Text('CP ' +
                                  cpsToDisplay[position].id.toString())),
                          title: _getPanelTitle(cpsToDisplay[position]),
                          initiallyExpanded: false,
                          children: [
                            CpDetailsBody(
                              cp: cpsToDisplay[position],
                              distanceUnit:
                                  distanceUnitChangeNotifier.distanceUnit,
                            ),
                          ],
                        );
                      }),
                    );
                  },
                ),
              ),
            ],
          );
        } else {
          // Don't show a proper loading view because the short blue loading
          // indicator is more confusing than an empty screen
          return Container();
        }
      },
    );
  }

  Widget _getPanelTitle(Cp cp) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Text(cp.name),
          ),
          cp.getIconsWidget(context),
        ],
      ),
    );
  }

  /// Filters through the list of all CPs and only adds those CPs to the display
  /// that satisfy a certain property
  /// Does NOT call setState, the caller needs to do that himself!
  List<Cp> _filterCpList(List<Cp> cps, CpFilterOption option) {
    List<Cp> newCpsToDisplay = [];
    switch (option) {
      case CpFilterOption.Supporters:
        cps.forEach((Cp cp) {
          if (cp.supporterAllowed) {
            newCpsToDisplay.add(cp);
          }
        });
        break;
      case CpFilterOption.Live:
        cps.forEach((Cp cp) {
          if (cp.liveTiming) {
            newCpsToDisplay.add(cp);
          }
        });
        break;
      default:
        newCpsToDisplay = cps;
        break;
    }
    return newCpsToDisplay;
  }
}
