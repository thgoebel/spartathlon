import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:spartathlon_app/views/course/CpFilterOption.dart';

class CourseListHeaderBar extends StatelessWidget {
  CourseListHeaderBar({
    required this.selectedOption,
    required this.onCpFilterOptionChanged,
  });

  final CpFilterOption selectedOption;
  final Function(CpFilterOption?) onCpFilterOptionChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 12.0, right: 12.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Theme.of(context).dividerColor, width: 1.5),
        ),
      ),
      child: Center(
        child: Row(
          children: [
            Expanded(
              child: Text(
                SpartaLocalizations.of(context).titleCheckpoints,
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            DropdownButtonHideUnderline(
              child: DropdownButton(
                value: selectedOption,
                hint: Text(
                  SpartaLocalizations.of(context).labelButtonFilterCheckpoints,
                ),
                items: allCpFilterOptions
                    .map((CpFilterOption o) => DropdownMenuItem(
                          value: o,
                          child: Text(o.toHumanString(context)),
                        ))
                    .toList(),
                onChanged: onCpFilterOptionChanged,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
