import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:spartathlon_app/models/AppData.dart';
import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/models/BlogPost.dart';
import 'package:spartathlon_app/models/Event.dart';
import 'package:spartathlon_app/providers/AthleteProvider.dart';
import 'package:spartathlon_app/providers/BlogProvider.dart';
import 'package:spartathlon_app/providers/EventProvider.dart';
import 'package:spartathlon_app/util/ApiCache.dart';
import 'package:spartathlon_app/util/Utils.dart';

// See https://ericwindmill.com/posts/inherited_widget/ for an intro to InheritedWidget

/// Root widget for the Spartathlon app.
///
/// It holds the data that needs to be
/// shared across the app (like the list of athletes) in its state, which can
/// be accessed from throughout the app since its an InheritedWidget.
class AppStateContainer extends StatefulWidget {
  final Widget child;

  AppStateContainer({required this.child});

  @override
  AppStateContainerState createState() => AppStateContainerState();

  static AppStateContainerState of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<_InheritedAppStateContainer>()! // TODO safety?
        .data;
  }
}

class AppStateContainerState extends State<AppStateContainer> {
  AppData data = AppData();

  @override
  void initState() {
    super.initState();
    loadData(false);
  }

  @override
  Widget build(BuildContext context) {
    return _InheritedAppStateContainer(data: this, child: widget.child);
  }

  /// Evicts the cache no matter of the current network connection state
  Future<void> evictCache() async {
    print('Ignoring network and clearing cache...\n');
    await spartaCacheManager.emptyCache();
    await DefaultCacheManager().emptyCache();
    setState(() {
      data.reset();
      loadData(false);
    });
  }

  /// Load all data for the app (athletes, blog entries, etc)
  ///
  /// If and only if a network connection is available, [forceReload] will empty
  /// the cache and load all data from the API again.
  /// ONLY empties athlete and event data NOT blog data (for performance reasons)
  ///
  /// Otherwise [forceReload] has no effect.
  Future<void> loadData(bool forceReload) async {
    print('Reloading data');

    bool hasConn = await hasConnectivity();
    forceReload =
        forceReload && hasConn; // no point in forcing if it won't succeed

    setState(() {
      data.isLoading = true;
      data.hasError = false;
      data.hasConnection = hasConn;
    });

    // Try and fetch all data for the app
    try {
      List<List> futureData = await Future.wait(
        [
          AthleteProvider.getAthletes(forceReload),
          BlogProvider.getBlogPosts(forceReload),
          EventProvider.getEvents(spartaCacheManager, forceReload: forceReload),
        ],
        eagerError: true,
      );

      List<Athlete> athletes = futureData[0].cast<Athlete>();
      List<BlogPost> blogPosts = futureData[1].cast<BlogPost>();
      List<Event> events = futureData[2].cast<Event>();

      // Separate this since it goes to disk and (usually) returns quickly
      List<Athlete> favoriteAthletes =
          await AthleteProvider.getFavoriteAthletes(athletes);

      setState(() {
        data.athletes = athletes;
        data.favoriteAthletes = favoriteAthletes;
        data.blogPosts = blogPosts;
        data.events = events;
      });
    } catch (error) {
      print('Error loading data: ' + error.toString());
      // Tell Flutter to rebuild and tell the widget that an error occurred.
      setState(() => data.hasError = true);
    }

    // Tell Flutter to rebuild and tell the widget that loading has finished.
    print('Finished loading data');
    setState(() => data.isLoading = false);
  }

  /// Helper function without arguments to call [loadData], forcing all data to
  /// be loaded again
  void reloadAfterError() {
    loadData(true);
  }

  void setFavoriteAthletes(List<Athlete> favoriteAthletes) {
    data.favoriteAthletes = favoriteAthletes;
  }

  void setToggleFavoriteAthlete(String id) {
    AthleteProvider.setToggleAthleteFavoriteToPrefs(id);
    Athlete? a = AthleteProvider.getAthleteById(data.athletes, id);

    if (a != null) {
      if (data.favoriteAthletes.contains(a)) {
        setState(() {
          data.favoriteAthletes.remove(a);
        });
      } else {
        setState(() {
          data.favoriteAthletes.add(a);
        });
      }
    }
    data.favoriteAthletes.sort();
  }

  Future<void> checkConnectionState() async {
    bool newHasConn = await hasConnectivity();
    if (newHasConn != data.hasConnection) {
      setState(() => data.hasConnection = newHasConn);
      // Force evict cache and download new data because we could have been
      // offline during a crucial stage of the race
      if (newHasConn) {
        loadData(true);
      }
    }
  }
}

class _InheritedAppStateContainer extends InheritedWidget {
  final AppStateContainerState data;

  _InheritedAppStateContainer({
    Key? key,
    required this.data,
    required Widget child,
  }) : super(key: key, child: child);

  // TODO Be more clever about checking whether the state has actually changed
  // and widget reliant on us need to be notified to rebuild
  @override
  bool updateShouldNotify(_InheritedAppStateContainer old) => true;
}
