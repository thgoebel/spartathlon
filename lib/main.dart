import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_gen/gen_l10n/sparta_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:spartathlon_app/AppStateContainer.dart';
import 'package:spartathlon_app/models/DistanceUnitChangeNotifier.dart';
import 'package:spartathlon_app/models/ThemeModeChangeNotifier.dart';
import 'package:spartathlon_app/providers/ThemeProvider.dart';
import 'package:spartathlon_app/util/DistanceUnit.dart';
import 'package:spartathlon_app/util/Themes.dart';
import 'package:spartathlon_app/views/MainPage.dart';

void main() {
  debugPaintSizeEnabled = false;
  runApp(Main());
}

class Main extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<dynamic>(
      // Get the user's preferences once (as a future).
      // Later on manage it through the Providers/ChangeNotifiers.
      future: _loadInitialPrefs(),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return _buildApp(snapshot.data![0], snapshot.data![1]);
        } else {
          return Container(color: spartaBlue);
        }
      },
    );
  }

  Widget _buildApp(
      ThemeMode initialThemeMode, DistanceUnit initialDistanceUnit) {
    print('Building app'); // should only be called once??

    // Providers + AppStateContainer needs to be above the MaterialApp in order
    // to be available across routes. See:
    // https://stackoverflow.com/questions/49968817/flutter-inherited-widget-and-routes
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeModeChangeNotifier>(
            create: (_) => ThemeModeChangeNotifier(initialThemeMode)),
        ChangeNotifierProvider<DistanceUnitChangeNotifier>(
            create: (_) => DistanceUnitChangeNotifier(initialDistanceUnit)),
      ],
      child: Consumer<ThemeModeChangeNotifier>(
        builder: (context, themeModeChangeNotifier, child) {
          // should only be build on themeMode changes
          print('(Re)building MaterialApp');

          return AppStateContainer(
            child: MaterialApp(
              title: 'Spartathlon',
              theme: lightTheme,
              darkTheme: darkTheme,
              themeMode: themeModeChangeNotifier.themeMode,
              localizationsDelegates: [
                SpartaLocalizations.delegate,
                // These should be already contained in the auto-generated SpartaLocalizations, but let's be double sure
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: SpartaLocalizations.supportedLocales,
              home: MainPage(),
            ),
          );
        },
      ),
    );
  }

  Future _loadInitialPrefs() {
    return Future.wait(
      [
        ThemeProvider.getThemeModeFromPrefs(),
        getDistanceUnitFromPrefs(),
      ],
      eagerError: true,
    );
  }
}
