import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:spartathlon_app/util/DistanceUnit.dart';

class DistanceUnitChangeNotifier extends ChangeNotifier {
  DistanceUnitChangeNotifier(this._distanceUnit);

  DistanceUnit _distanceUnit;

  DistanceUnit get distanceUnit => _distanceUnit;

  set distanceUnit(DistanceUnit newDistanceUnit) {
    _distanceUnit = newDistanceUnit;
    setDistanceToPrefs(newDistanceUnit);
    notifyListeners();
  }
}
