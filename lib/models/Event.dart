/// Event model
class Event extends Comparable<Event> {
  final DateTime from;
  final DateTime? to;
  final String coordinates;
  final String location;
  final String description;

  Event({
    required this.from,
    required this.to,
    required this.coordinates,
    required this.location,
    required this.description,
  });

  factory Event.fromJson(Map<String, dynamic> json) {
    // The '+03:00' is appended to tell the parser that the time is in UTC+3.
    // The API guarantees us that times will be in Greek-local time.
    return Event(
      from: DateTime.parse(json["date"] + 'T' + json["timeFrom"] + '+03:00'),
      to: (json["timeTo"] != null)
          ? DateTime.parse(json["date"] + 'T' + json["timeTo"] + '+03:00')
          : null,
      coordinates: json["coordinates"].toString(),
      location: json["location"].toString(),
      description: json["description"].toString(),
    );
  }

  @override
  int compareTo(Event other) {
    return from.compareTo(other.from);
  }

  @override
  String toString() {
    return "Event from " +
        from.toString() +
        " until " +
        to.toString() +
        " called " +
        description;
  }

  bool isOnSameDayOrLater(DateTime other) {
    DateTime eventDate =
        DateTime.utc(this.from.year, this.from.month, this.from.day);
    DateTime nowDate = DateTime.utc(other.year, other.month, other.day);

    return eventDate.compareTo(nowDate) >= 0;
  }
}
