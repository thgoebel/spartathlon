import 'package:spartathlon_app/providers/AthleteProvider.dart';

/// Athlete Model
class Athlete extends Comparable<Athlete> {
  final String id;
  final String firstName;
  final String lastName;
  final String name;
  String nation;
  String dateOfBirth;
  final String imageUrl;
  String biography;
  List<Finish> finishes;
  int bibNumber;
  String lastSeenTime;
  int lastSeenCp;
  bool isFullyLoaded;

  Athlete({
    required this.id,
    required this.firstName,
    required this.lastName,
    required this.name,
    required this.nation,
    required this.dateOfBirth,
    required this.imageUrl,
    required this.biography,
    required this.finishes,
    required this.bibNumber,
    required this.lastSeenTime,
    required this.lastSeenCp,
    this.isFullyLoaded = false,
  });

  factory Athlete.fromJson(Map<String, dynamic> json) {
    List<Finish> finishes = <Finish>[];

    /// When created in [getAthletes] the following fields will be null:
    /// dateOfBirth, biography, finishes
    /// since the API only returns them on a more detailed call.
    ///
    /// For efficiency reasons, we only do so in [getFullAthleteDetails] when
    /// the user explicitly opens the details for an athlete.
    /// TODO Use a better backend that includes these fields already
    return Athlete(
      id: json['atid'].toString(),
      firstName: json['firstname'].trim(),
      lastName: json['surname'].trim(),
      name: (json['firstname'] + ' ' + json['surname']).trim(),
      // Remove whitespaces
      nation: json['country'],
      dateOfBirth: json['birthdate'],
      imageUrl:
          'https://spartathlon.gr/en/races/races.html?task=athlete.getImage&id=' +
              json['atid'],
      biography: json['minicv'],
      finishes: finishes,
      bibNumber: (json['number'] != null) ? int.parse(json['number']) : 0,
      lastSeenTime: json['datetime'],
      // API starts at 1 leading to Sparta being CP 76. We do 0-indexing.
      lastSeenCp: (json['cpid'] != null) ? (int.parse(json['cpid']) - 1) : 0,
      isFullyLoaded: false,
    );
  }

  @override
  int compareTo(Athlete other) {
    return lastName.toLowerCase().compareTo(other.lastName.toLowerCase());
  }

  Future<bool> isFavorite() async {
    List<String> favIds = await AthleteProvider.getFavoriteAthleteIds();
    return favIds.contains(id);
  }
}

class Finish {
  final String year;
  final String bib;
  final String time;

  // TODO Maybe add a field for a place/position

  Finish({
    required this.year,
    required this.bib,
    required this.time,
  });
}
