import 'package:spartathlon_app/models/Athlete.dart';
import 'package:spartathlon_app/models/BlogPost.dart';
import 'package:spartathlon_app/models/Event.dart';

/// Hold state of the app, i.e. list of all athletes, all favorite athletes and
/// all blog entries.
class AppData {
  bool isLoading = true;
  bool hasError = false;
  bool hasConnection = true;

  List<Athlete> athletes = [];
  List<Athlete> favoriteAthletes = [];
  List<BlogPost> blogPosts = [];
  List<Event> events = [];

  @override
  String toString() {
    return '### vvvvvvvv\nisLoading: ' +
        isLoading.toString() +
        '\nhasError: ' +
        hasError.toString() +
        '\nblogEntries: ' +
        blogPosts.toString() +
        '\nomitting athletes and favorite athletes and events' +
        '\n### ^^^^^^^^^\n';
  }

  void reset(){
    this.athletes = [];
    this.blogPosts = [];
    this.favoriteAthletes = [];
    this.events = [];
  }
}
