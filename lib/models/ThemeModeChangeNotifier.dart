import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:spartathlon_app/providers/ThemeProvider.dart';

class ThemeModeChangeNotifier extends ChangeNotifier {
  ThemeModeChangeNotifier(this._themeMode);

  ThemeMode _themeMode;

  ThemeMode get themeMode => _themeMode;

  set themeMode(ThemeMode newThemeMode) {
    _themeMode = newThemeMode;
    ThemeProvider.setThemeModeToPrefs(newThemeMode);
    notifyListeners();
  }
}
