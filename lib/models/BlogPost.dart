/// BlogPost Model
class BlogPost extends Comparable<BlogPost> {
  final String url;
  final int storyId;
  final String? imageUrl;
  final String title;
  final String teaserText;
  final String fullText;

  BlogPost({
    required this.url,
    required this.storyId,
    required this.title,
    required this.teaserText,
    required this.fullText,
    required this.imageUrl,
  });

  factory BlogPost.fromJson(Map<String, dynamic> json) {
    return BlogPost(
      url: json["url"],
      storyId: json["isa_id"],
      imageUrl: json["image_url"],
      title: json["title"],
      teaserText: json["teaser"],
      fullText: json["content"],
    );
  }

  @override
  String toString() {
    return 'Post $storyId: $title image=$imageUrl';
  }

  /// Sort by storyId descendingly (i.e. largest == latest story ID first).
  @override
  int compareTo(BlogPost other) {
    return other.storyId.compareTo(storyId);
  }
}
