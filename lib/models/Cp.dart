import 'package:flutter/material.dart';

/// Checkpoint (aka CP) Model
class Cp {
  final int id;
  final String name;
  final double fromStart; // in km
  final double toNext; // in km
  final double toFinish; // in km
  final String openingTime;
  final String closingTime;
  final bool liveTiming;
  final bool supporterAllowed;
  final bool firstAid;
  final bool isMajor;
  final bool isTown;
  final double long;
  final double lat;
  final String supplies;
  final String description;

  /// Creates a new CP.
  ///
  /// Distances [fromStart], [toNext] and [toFinish] must be given in kilometer
  Cp({
    required this.id,
    required this.name,
    required this.fromStart,
    required this.toFinish,
    required this.toNext,
    required this.openingTime,
    required this.closingTime,
    required this.liveTiming,
    required this.supporterAllowed,
    required this.firstAid,
    required this.isMajor,
    required this.isTown,
    required this.long,
    required this.lat,
    required this.supplies,
    required this.description,
  });

  /// String to draw onto the elevation profile
  String get profileLabel => isTown ? "$name" : "#$id";

  /// Returns a widget containing the icons that describe this CP,
  /// e.g. whether it has live timing.
  ///
  /// TODO make this a standalone widget?
  /// TODO add a semantic label for accessibility
  Widget getIconsWidget(BuildContext context) {
    List<Widget> iconList = [
      Padding(padding: EdgeInsets.only(left: 4)),
    ];

    if (firstAid) {
      iconList.add(Padding(
        padding: EdgeInsets.only(left: 4),
        child: Icon(
          Icons.local_hospital_outlined,
          color: Theme.of(context).iconTheme.color,
        ),
      ));
    }
    if (liveTiming) {
      iconList.add(Padding(
          padding: EdgeInsets.only(left: 4),
          child: Icon(
            Icons.insert_chart_outlined,
            color: Theme.of(context).iconTheme.color,
          )));
    }
    if (supporterAllowed) {
      iconList.add(Padding(
          padding: EdgeInsets.only(left: 4),
          child: Icon(
            Icons.group_outlined,
            color: Theme.of(context).iconTheme.color,
          )));
    }

    return Row(
      children: iconList,
    );
  }

  @override
  String toString() => "CP$id $name Km $fromStart";
}
