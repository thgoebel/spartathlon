# Changelog

Release dates refer to the date where the release was pushed to the `main` branch on Gitlab.
Actual release dates for the different stores may be later.

## Unreleased

[NEW]
* Browse the race course's elevation profile and visually explore how far checkpoints are apart (in distance as well as in elevation)

[CHANGED]
* Color theme follows the devices system by default
* Modernised codebase, now building with null safety!

[FIXED]
* Scroll position not saved when switching tabs
* Blog posts can have no header image

## Version 0.8.2 (2019-09-15)

[CHANGED]
* Lightened dark theme ever so slightly (aligns with Material Design guidelines)

[FIXED]
* Crash on text selection on iOS on non-English languages
* Results screen not centered on tablets

## Version 0.8.1 (2019-09-13)

[CHANGED]
* Design improvements (paddings, border thicknesses)

## Version 0.8.0 (2019-09-12)

[NEW]
* Added Greek translation
* Remember last used checkpoint filter
* Text in loading screen

[CHANGED]
* Made dark theme really dark, not just grey (because OLED)
* Different race description in About
* Removed reference to Gitlab issue in Results screen
* Performance improvements

## Version 0.7.0 (2019-08-08)

[NEW]
* Added race schedule to the app

## Version 0.6.0 (2019-07-19)

[NEW]
* German locale

[FIXED]
* Workaround for country of residence instead of country of origin being shown for some athletes
* Better performance when opening blog articles

[CHANGED]
* Prepared the code for translations. Reach out if you speak a language that has not yet been added!
* Upgraded dependencies

## Version 0.5.0 (2019-06-21)

[NEW]
* Choose between km and miles
* Search athletes by their bib number
* Show warning that data may not be up-to-date when no network connection is available
* Added privacy policy
* Option to clear the cache
* Various tooltips for buttons

[CHANGED]
* Only show live data on dashboard and athlete details during the race
* Improved caching (refresh cache more regularly during and after the race)
* Upgraded dependencies and moved to AndroidX

[FIXED]
* Various regressions in new versions of Flutter or package dependencies

## Version 0.4.0 (2019-05-01)

[NEW]
* Pull-to-refresh on the dashboard and the athlete list

[CHANGED]
* Improved AthleteDetailsView design: make better use of space by only using half the
  screen height for image, redesign date of birth/country layout

[FIXED]
* Athlete name in details view not aligned to the left when expanded (#6)
* Keyboard not shown in athlete search field (#13)
* No error shown when network and no cached data available (#14)

## Version 0.3.2 (2019-03-06)

[NEW]
* Feature graphic for F-Droid

[FIXED]
* No text but black screen in blog view
* Round corners in About view
* Screenshots for F-Droid

## Version 0.3.1 (2019-03-04)

[FIXED]
* Crash on Android 9

## Version 0.3.0 (2019-02-23)
[NEW]
* Screenshots for F-Droid
* New build flavor *play* (just a dummy for now)

[CHANGED]
* Corners of cards and dialogs are more round to align with Material 2 standards

## Version 0.2.0 (2019-02-17)
[CHANGED]
* Changed icon set to outlined Material
* Make settings accessible more quickly


## Version 0.1 (2019-02-10)
First release of the Spartathlon app to Gitlab

[NEW]
* Read race reports, follow the checkpoints and search for athletes
* Dark theme
* Mark athletes as favourite
