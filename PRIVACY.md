# Privacy Policy

Last updated: 2019-07-28

This is the privacy policy for the Spartathlon app ("the app") developed by Thore Goebel
("the developers").

## Internet access

The app needs internet access to download data like the list of athletes or the event schedule.
You can disallow the app internet access in your phone's settings, but in doing so the app's
functionality will be greatly restricted.

Currently the app contacts the servers behind these two domains:

1) "spartathlon.gr", operated by the International Spartathlon Association (ISA)
2) "spartathlon.app", operated by the developers of this app.

The app only downloads data, it does not upload personal data to these servers.

By using this app, you expose similar data as if you were viewing these sites in your browser.
Please refer to their respective privacy policies for further information.

## Analytics

The app does not collect any usage data and does not deploy any dedicated analytics tools.

Keep in mind that if you download the app through the App Store or Google Play, then both the
app developers and the store operator have access to the store's download statistics.