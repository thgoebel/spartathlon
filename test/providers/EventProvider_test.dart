import 'dart:io' as io;

import 'package:file/file.dart' as f;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:spartathlon_app/models/Event.dart';
import 'package:spartathlon_app/providers/EventProvider.dart';
import 'package:spartathlon_app/util/ApiCache.dart';
import 'package:spartathlon_app/util/Utils.dart';

class MockCacheManager extends Mock implements CacheManager {}

/// EventProvider tests using the data in 'stubs/timetable.json'
void main() {
  return;
  // TODO Fix mocking
  // ignore_for_file: dead_code
  final MockCacheManager mockCacheManager = MockCacheManager();

  final String mockPath =
      io.Platform.environment['PWD']! + '/test/stubs/timetable.json';

  when(mockCacheManager.getSingleFile(API_URL_EVENTS))
      .thenReturn(Future<f.File>.value(io.File(mockPath) as f.File));

  final FileInfo mockFileInfo = FileInfo(
    io.File(mockPath) as f.File,
    FileSource.NA,
    DateTime.now().add(Duration(days: 1)),
    'originalUrl',
  );
  when(mockCacheManager.downloadFile(API_URL_EVENTS))
      .thenReturn(Future<FileInfo>.value(mockFileInfo));

  group('hasCurrentOrFutureEvents', () {
    // Groups cannot be async

    test('long before 2019 race', () async {
      DateTime date = DateTime(2019, 1, 1);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(EventProvider.hasCurrentOrFutureEvents(events, date), true);
    });
    test('before 2019 race', () async {
      DateTime date = DateTime(2019, 9, 24);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(EventProvider.hasCurrentOrFutureEvents(events, date), true);
    });
    test('during 2019 race', () async {
      DateTime date = DateTime(2019, 9, 27);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(EventProvider.hasCurrentOrFutureEvents(events, date), true);
    });
    test('end of 2019 race', () async {
      DateTime date = DateTime(2019, 10, 1);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(EventProvider.hasCurrentOrFutureEvents(events, date), true);
    });
    test('after 2019 race', () async {
      DateTime date = DateTime(2019, 10, 2);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(EventProvider.hasCurrentOrFutureEvents(events, date), false);
    });
    test('beginning of 2020', () async {
      DateTime date = DateTime(2020, 09, 1);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(EventProvider.hasCurrentOrFutureEvents(events, date), false);
    });
  });

  group('getEventsPerDay', () {
    test('basic', () async {
      DateTime now = DateTime(2019, 1, 1);
      List<Event> events = await EventProvider.getEvents(mockCacheManager);
      expect(validEventsPerDayList(EventProvider.getEventsPerDay(events, now)),
          true);
    });
  });
}

// Helper function to determine whether the list of lists of events satisfies
// our constraints
bool validEventsPerDayList(List<List<Event>> events) {
  List<DateTime> days = [];

  // Test that all events in sublist are on the same day
  for (List<Event> eventsOnDay in events) {
    DateTime day = eventsOnDay.first.from;
    days.add(day);
    eventsOnDay.removeWhere((Event e) => sameDay(e.from, day));
    if (eventsOnDay.isNotEmpty) {
      print('ERROR: A sublist contained events on different days!');
      return false;
    }
  }

  // Test that the days were sorted
  List<DateTime> sorted = List<DateTime>.from(days);
  sorted.sort((a, b) => a.compareTo(b));
  for (int i = 0; i < days.length; i++) {
    if (days[i] != sorted[i]) {
      print('ERROR: The list of days was not sorted!');
      return false;
    }
  }

  // Test that there are no two sublists for the same day
  for (int i = 0; i < days.length; i++) {
    int numDays = 0;
    for (int j = 0; i < days.length; i++) {
      if (sameDay(days[i], days[j])) {
        numDays++;
      }
    }
    if (numDays != 1) {
      print('ERROR: There were two sublists for the same days!');
      return false;
    }
  }

  return true;
}
