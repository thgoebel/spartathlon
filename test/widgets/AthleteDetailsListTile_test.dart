import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:spartathlon_app/widgets/AthleteDetailsListTile.dart';

void main() {
  testWidgets('AthleteDetailsListTile', (WidgetTester tester) async {
    // Wrap the widget in a MaterialApp to have directionality (for icon and text)
    await tester.pumpWidget(
      MaterialApp(
        home: AthleteDetailsListTile(
          leading: Icon(Icons.public),
          supertitle: Text('Country'),
          title: Text('Greece'),
        ),
      ),
    );

    expect(find.byIcon(Icons.public), findsOneWidget);
    expect(find.text('Country'), findsOneWidget);
    expect(find.text('Greece'), findsOneWidget);
  });
}
