import 'package:platform/platform.dart';
import 'package:spartathlon_app/util/Utils.dart';
import 'package:test/test.dart';

void main() {
  // TODO test web
  group('getGeoUrl', () {
    String lat = '37.0790590';
    String long = '22.4284285';
    test('android', () {
      expect(getGeoUrl(lat, long, FakePlatform(operatingSystem: 'android')),
          'geo:37.0790590,22.4284285?q=37.0790590,22.4284285');
    });
    test('iOS', () {
      expect(getGeoUrl(lat, long, FakePlatform(operatingSystem: 'ios')),
          'maps:37.0790590,22.4284285?q=37.0790590,22.4284285');
    });
  });

  group('spartafyUrl', () {
    test('normal http', () {
      String url = 'http://spartathlon.app/api/news/';
      expect(spartafyUrl(url), url);
    });
    test('normal https', () {
      String url = 'https://spartathlon.app/api/news/';
      expect(spartafyUrl(url), url);
    });
    test('normal www', () {
      String url =
          'www.spartathlon.gr/en/the-spartathlon-race-en/historical-information-en.html';
      expect(spartafyUrl(url), url);
    });
    test('relative url', () {
      String url = '/en/the-spartathlon-race-en/historical-information-en.html';
      String real =
          'https://www.spartathlon.gr/en/the-spartathlon-race-en/historical-information-en.html';
      expect(spartafyUrl(url), real);
    });
  });

  group('sameDay', () {
    DateTime now = DateTime.now();

    test('Datetime.now()', () {
      expect(sameDay(now, now), true);
    });
    test('a earlier than b', () {
      expect(sameDay(now.subtract(Duration(days: 7)), now), false);
    });
    test('b earlier than a', () {
      expect(sameDay(now, now.subtract(Duration(days: 7))), false);
    });
    test('same day but a year apart', () {
      expect(sameDay(now, now.add(Duration(days: 365))), false);
    });
  });
}
