import 'package:clock/clock.dart';
import 'package:test/test.dart';

import 'package:spartathlon_app/util/RaceTiming.dart';

void main() {
  // IMPORTANT: ALWAYS USE UTC!
  group('function_isBetween', () {
    test('between', () {
      DateTime now = DateTime.now().toUtc();
      bool isBetw = isBetween(now, now.subtract(Duration(seconds: 1)),
          now.add(Duration(seconds: 1)));
      expect(isBetw, true);
    });
    test('not between', () {
      DateTime now = DateTime.now().toUtc();
      bool isBetw = isBetween(now, now.add(Duration(seconds: 1)),
          now.subtract(Duration(seconds: 1)));
      expect(isBetw, false);
    });
    test('exactly between', () {
      DateTime now = DateTime.now().toUtc();
      bool isBetw = isBetween(now, now, now);
      expect(isBetw, false);
    });
  });

  group('function_isRaceOn', () {
    test('just after start', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 27, 4, 1));
      expect(isRaceOn(clock: mockClock), true);
    });
    test('middle of race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 27, 19));
      expect(isRaceOn(clock: mockClock), true);
    });
    test('just before end', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 28, 15, 59));
      expect(isRaceOn(clock: mockClock), true);
    });

    test('not before start', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 27, 4, 0));
      expect(isRaceOn(clock: mockClock), false);
    });
    test('not at end', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 28, 16, 0));
      expect(isRaceOn(clock: mockClock), false);
    });
    test('not after end', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 28, 16, 1));
      expect(isRaceOn(clock: mockClock), false);
    });
    test('Not at Christmas', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 12, 24));
      expect(isRaceOn(clock: mockClock), false);
    });
  });

  group('function_isRaceWeek', () {
    test('Monday before race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 23));
      expect(isRaceWeek(clock: mockClock), true);
    });
    test('Thursday before race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 26));
      expect(isRaceWeek(clock: mockClock), true);
    });
    test('Friday of race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 27));
      expect(isRaceWeek(clock: mockClock), true);
    });
    test('Sunday after race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 29));
      expect(isRaceWeek(clock: mockClock), true);
    });
    test('Tuesday before race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 30));
      expect(isRaceWeek(clock: mockClock), true);
    });

    test('Not two weeks before race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 20));
      expect(isRaceWeek(clock: mockClock), false);
    });
    test('Not a week after race', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 10, 4));
      expect(isRaceWeek(clock: mockClock), false);
    });
    test('Not at Christmas', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 12, 24));
      expect(isRaceWeek(clock: mockClock), false);
    });
  });

  group('function_isTwoWeeksAfterRace', (){
    test('Not before race', (){
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 25));
      expect(isTwoWeeksAfterRace(clock: mockClock), false);
    });
    test('Not during race', (){
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 27));
      expect(isTwoWeeksAfterRace(clock: mockClock), false);
    });
    test('Just after the race', (){
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 28, 16, 1));
      expect(isTwoWeeksAfterRace(clock: mockClock), true);
    });
    test('Monday after the race', (){
      final mockClock = Clock.fixed(DateTime.utc(2019, 9, 30));
      expect(isTwoWeeksAfterRace(clock: mockClock), true);
    });
    test('Two weeks after race', (){
      final mockClock = Clock.fixed(DateTime.utc(2019, 10, 11));
      expect(isTwoWeeksAfterRace(clock: mockClock), true);
    });
    test('Not three weeks after race', (){
      final mockClock = Clock.fixed(DateTime.utc(2019, 10, 18));
      expect(isTwoWeeksAfterRace(clock: mockClock), false);
    });
    test('Not at Christmas', () {
      final mockClock = Clock.fixed(DateTime.utc(2019, 12, 24));
      expect(isTwoWeeksAfterRace(clock: mockClock), false);
    });
  });
}
