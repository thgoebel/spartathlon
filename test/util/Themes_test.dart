import 'package:spartathlon_app/util/Themes.dart';
import 'package:test/test.dart';

void main() {
  group('function_isLight', () {
    test('light theme is light', () {
      expect(isLight(lightTheme), true);
    });
    test('dark theme is not light', () {
      expect(isLight(darkTheme), false);
    });
  });
}
