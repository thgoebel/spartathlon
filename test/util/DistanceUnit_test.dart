import 'package:test/test.dart';

import 'package:spartathlon_app/util/DistanceUnit.dart';

void main() {
  // The order is important because the index values are used for storing the
  // user's distance preference in SharedPreferences.
  // Thus if the order is changed across versions, it will mess up users' settings
  group('enum order', () {
    test('km', () {
      expect(DistanceUnit.values[0], DistanceUnit.km);
    });
    test('miles', () {
      expect(DistanceUnit.values[1], DistanceUnit.miles);
    });
  });

  group('function_getDistanceString', () {
    test('km to km', () {
      String s = getDistanceString(null, 1.0, DistanceUnit.km);
      expect(s, '1.0 km');
    });
    test('km to km', () {
      String s = getDistanceString(null, 1, DistanceUnit.km);
      expect(s, '1.0 km');
    });
    test('km to miles', () {
      String s = getDistanceString(null, 1.0, DistanceUnit.miles);
      expect(s, '0.6 miles');
    });
    test('km to miles', () {
      String s = getDistanceString(null, 1.609, DistanceUnit.miles);
      expect(s, '1.0 miles');
    });
  });
}
