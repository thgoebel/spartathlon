# Contributing

This document outlines in what areas and in what ways you can contribute to the Spartathlon app.


## Feedback and ideas

Since we don't run statistics tools we rely on your feedback to tell us how to make this app better.
Better for you and better for all the other Spartathletes, supporters and volunteers.

1) How did the app help you?
2) What functionality did you miss?
3) What problems did you have when using the app?

Reach us at [contact@spartathlon.app](mailto:contact@spartathlon.app) or open an issue on Gitlab.


## Translations

Translations are managed on [POEditor](https://poeditor.com/).
Contact us to get an invite and help translating!
This can be adding new languages or improving existing translations.

### Tiny changes

For minor translation fixes (e.g. typos) for which you don't want to open an account on POEditor,
you can also simply open an issue or email us.
Just make sure to state the following information:

* Where the wrong text appears in the app
* The language
* The wrong text
* The correct text

### Notes for developer

Flutter uses the [ARB format](https://github.com/google/app-resource-bundle/wiki/ApplicationResourceBundleSpecification)
to store translations.

1) Fork the project and make sure it runs locally
2) Read https://flutter.dev/docs/development/accessibility-and-localization/internationalization
3) Export the translations from POEditor and put them into `lib/l10n`
4) For **new** languages, make sure to also add them to `ios/Runner/Info.plist`
5) Flutter **should** generate the required code automatically from all ARB files, see `l10n.yml`.
6) Verify on both Android and iOS that the translations are applied correctly (the app follows your system language)

Gotchas:

- Do NOT use the *context* feature on POEditor. It doesn't work when exporting to ARB.
- Flutter requires a resource attribute (`@resourceName`) for each resource.
However, POEditor only exports one if there is a comment with the term.
So, just put the context into the comment, and all is fine.

## Code

This app is built with [Flutter](https://flutter.dev/).
Getting familiar with Dart and Flutter will be a good starting point if you want to contribute code.

1) Install Flutter: https://flutter.dev/docs/get-started/install
2) Fork the project to your own Gitlab account.
3) Clone the code: `git clone git@gitlab.com:<YOURUSERNAME>/spartathlon.git`
4) Open the project in AndroidStudio.
5) To run it, edit your *run configuration* to use the build flavor "play".
   Alternatively from the command line call `flutter run --flavor=play`.

You might need additional setup steps (like installing the Flutter plugin for AndroidStudio or ADB
to run it on a physical device) that have been omitted here because they are not project-specific.

All development work is done on the `dev` branch.
On your fork of the project, create a new branch from there and direct your MR there, too.

If you want to implement larger features, we ask you to open an issue first so we can discuss
the design before you start putting a lot of work into it.

If you have any dev related questions feel free to email us at contact@spartathlon.app.

### Common pitfalls:

- `Exception: Gradle build failed to produce an .apk file.
   It's likely that this file was generated under /home/username/workspace/spartathlon/build, but the tool couldn't find it.`
   This is due to the project having multiple build flavors. Usually you want to run the `play`flavor.
   Either run from the command line with `flutter run --flavor=play` or edit the run configuration in Android Studio accordingly.
