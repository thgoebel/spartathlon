# Spartathlon App (Unofficial)

Mobile app built with [Flutter](https://flutter.dev/) for the Greek ultra marathon race [Spartathlon](www.spartathlon.gr/en).

It is built with three audiences in mind: Spartathletes, support crews, and volunteers.
It aims to help them before, during and after the race in the getting information they need in a clean,
modern and mobile-friendly way.

Note: This app is **NOT** (yet) officially endorsed by the International Spartathlon Association ISA.

## Get the app

<p align="center"><a href="https://f-droid.org/app/ch.thgoebel.spartathlonapp"><img src="https://f-droid.org/badge/get-it-on.png" height="75"></a></p> 

Releases for Google Play and Apple's App Store will follow.

## Features

* Read race reports
* Browse the race schedule and quickly get to the next event
* View checkpoints, get directions and filter by "supporters allowed" or "live results available"
* View the race course's elevation profile
* Search through the list of the athletes who compete(d) in the current year.
* Display your favourite athletes directly on the dashboard and easily follow their race progress
* Dark theme (easier on the eyes if you're waiting for your athlete in Nestani)

This covers what is in the [old app](https://play.google.com/store/apps/details?id=gr.netfocus.spartathlon) - and more!
We aim to increase the functionality and make more features available (such as viewing results for past years).
However, this often requires server-side changes, and with Spartathlon being fully run by volunteers you
might need to be patient.

Also see the changelog [here](https://gitlab.com/thgoebel/spartathlon/blob/main/CHANGELOG.md).

## Screenshots

[<img src="https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_dashboard.png" width=200>](https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_dashboard.png)
[<img src="https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_course.png" width=200>](https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_course.png)
[<img src="https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_elevation_profile.png" width=200>](https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_elevation_profile.png)
[<img src="https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_athlete_list.png" width=200>](https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_athlete_list.png)
[<img src="https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_athlete_details.png" width=200>](https://gitlab.com/thgoebel/spartathlon/raw/main/screenshots/screenshot_athlete_details.png)

## Contributing

Whether you're a developer or not, you can help make this app better!
See the [CONTRIBUTING.md](https://gitlab.com/thgoebel/spartathlon/blob/main/CONTRIBUTING.md) for details.

## About

Author: Thore Goebel

License: GNU General Public License v3 or later ([copy](https://gitlab.com/thgoebel/spartathlon/blob/main/LICENSE))
